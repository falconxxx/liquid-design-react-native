
package com.reactlibrary;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

import com.facebook.react.ReactPackage;
import com.facebook.react.bridge.NativeModule;
import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.uimanager.ViewManager;
import com.facebook.react.bridge.JavaScriptModule;
import com.entria.views.RNViewOverflowManager;
import com.horcrux.svg.SvgPackage;


public class LiquidDesignPackage implements ReactPackage {
    @Override
    public List<NativeModule> createNativeModules(ReactApplicationContext reactContext) {
      return Arrays.<NativeModule>asList(
        new LiquidDesignModule(reactContext)
      );
    }

    @Override
    public List<ViewManager> createViewManagers(ReactApplicationContext reactContext) {
        List<ViewManager> viewManagers = new ArrayList();
        List<ViewManager> svgViewManagers = new SvgPackage().createViewManagers(reactContext);
        viewManagers.addAll(svgViewManagers);
        viewManagers.add(new RNViewOverflowManager());
        return viewManagers;
    }
}
