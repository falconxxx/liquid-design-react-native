/* eslint-disable no-console */

const fs = require('fs')
const { exec } = require('child_process')

fs.readFileSync('.env', 'utf-8').split(/\r?\n/).forEach((line) => {
  if (line === 'APPLY_PATCHES=true') {
    exec('yarn patch-package', (error, stdout, stderr) => {
      if (error) {
        console.error(`exec error: ${error}`)

        return
      }
      console.log(`stdout: ${stdout}`)
      console.log(`stderr: ${stderr}`)
    })
  }
})
