### Installation
1. Clone the repo:
```bash
$ git clone git@gitlab.com:liquid-design/liquid-design-react-native.git
$ cd liquid-design-react-native
```

2. Change APPLY_PATCHES env variable (.env file) from true to false

3. Install dependencies:
```bash
$ yarn
```

4. Link dependencies:
```bash
$ react-native link
```

### Running Tests
```bash
$ yarn test
```

### Loki
In order to run Loki, you need to follow the instructions for starting Storybook first, then run:

```bash
$ yarn loki test ios/android
```

In order to update the references use command

```bash
$ yarn loki update
```

To review changes in screen check in the folder `loki/current` and `loki/difference`.
