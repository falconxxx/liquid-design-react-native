# Liquid-Design-System React Native

[![Gitter chat](https://badges.gitter.im/liquid-design-system/liquid-design-react-native.png)](https://gitter.im/liquid-design-system/liquid-design-react-native)

> Liquid Design System components for React Native. Design System and Component Kit to create beautifull applications.

## Terms of Use

see [TERMS_OF_USE.md](TERMS_OF_USE.md)

Please note that the security of the platform/project at which you are using Liquid Design System, or parts of it, are not the responsibility of the providers of this design system.

## Installation

This library depends on 3 other libraries: react-native-svg, react-native-vector-icons and react-native-view-overflow.
To use @liquid-design/liquid-design-react-native, please make sure that you have the other libraries added and linked correctly:
* [react-native-svg](https://github.com/react-native-community/react-native-svg)
* [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)
* [react-native-view-overflow](https://github.com/entria/react-native-view-overflow)

After installing the abovementioned libraries, please continue with:
```bash
npm install --save @liquid-design/liquid-design-react-native
```
or
```bash
yarn add @liquid-design/liquid-design-react-native
```
To link native dependencies after instalation run
```bash
react-native link
```
More details about linking libraries: [LINKING.md](LINKING.md)


## Usage
Import the component you need to use it.

```jsx
import React, { Component } from 'react'

import { Bubble } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Bubble/>
    )
  }
}
```

## Theming
Some components have themeName prop in which you can pass one of themes to use with the library.
Themes list: vibrantCyan, richBlue, richPurple, vibrantMagenta.
The default theme is vibrantCyan.
```jsx
import React, { Component } from 'react'

import { GhostButton } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <GhostButton themeName='vibrantMagenta' />
    )
  }
}
```

If you want to use your custom theme you can pass object instead of theme name. Custom theme should be an object with a specific shape passed as ThemeName prop.
```jsx
import React, { Component } from 'react'

import { GhostButton } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  const customTheme = {
    primary: {                      // overrides primary theme colors
      lightest: '#A5A5E6',
      light: '#A5A5E6',
      base: '#6767EF',
      dark: '#22226D',
      darker: '#3737A2',
    },
    secondary: {                    // overrides secondary theme colors
      lightest: '#D4F5EF',
      light: '#A5A5E6',
      base: '#31F3CF',
      dark: '#03FFD1'
      darker: '#3737A2',
    }
  }

  render() {
    return (
      <GhostButton themeName={customTheme} />
    )
  }
```

### Prerequisites
* [React Native](https://facebook.github.io/react-native/docs/getting-started.html)
* [Yarn](https://yarnpkg.com/en/docs/install)

### API Reference
* [Merck Design System](https://merck.design/)

##### This README will be updated before making library public.

# Elements

<details><summary><b>Bowls</b></summary>

## **Bowls**

>Bowls are graphic elements with fancy animation on changing value.

```jsx
import React, { Component } from 'react'

import { Bowl, Slider } from '@liquid-design/liquid-design-react-native'

class Example extends Component {
  state = {
    H: 0
  }

  render() {
    return (
      <Bowl H={this.state.H} />
      <Slider onSlidingComplete={val => this.setState({ H: val })}
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`H` | number | ` (isRequired` | Height of the waves inside Bowl - animation is on changing this value.
`looped` | bool | `false` | If set to true will loop animation.
</details>

<details><summary><b>Bubbles</b></summary>

## **Bubbles**

> Bubbles are notifications that indicate new available information at the data categorie’s respective link or icon.

Use bubbles as temporary and dynamic elements that adapt to new information and not as a static element. Indicators need to be used respective to the kind of available data.

```jsx
import React, { Component } from 'react'

import { Bubble } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Bubble />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`alignItems`        | string  | `'center'` | Align the Component on the Y axis.
`backgroundColor`   | string  | `'primary'`| Main color of the Component.
`borderRadius`      | number  | `50` | Border Radius of a Component. **May needed to be changed when size is changed.**
`color`             | string  | `'white'`  | Color of the Component's content element.
`disabled`          | bool    | `false`  | Disable / enable Component.
`fontSize`          | number  | `3`      | Font size of a Component content. **3 is the index number of fontSize from the theme file, where all the font sizes are declared**.
`fontWeight`        | number  | `6`      | Weight of a Component content. **3 is the index number of font weight from the theme file, where all the font weights are declared**
`justifyContent`    | string  | `'center'`| Style of Component on the X axis.
`size`              | number  | ` 30 `     | Size of the Component.
`iconSize`              | number  | ` 14 `     | Size of the Icon inside the Bubble.
`warning`           | bool    | ` false `  | Style for Component in warning state.
`textAlign`         | string  | `'center'`| Align of element inside Component.
</details>

<details><summary><b>Buttons</b></summary>

## **Buttons**
> Buttons, or call-to-actions (CTAs) are used for important user actions. Button labels should be speaking in the sense of the user’s understanding, rather than in the system’s language. Buttons can be combined with each other, but only within their label-groups (i.e. Text, Icon, Text & Icon).
>
### Primary Button
>Use primary buttons for high prioritized interactions as they should guide the user. Use only as stand-alone or in combination with secondary or ghost buttons.
### Secondary Button
>Use secondary buttons for low prioritized interactions that need to be available for the user (e.g. Back, Cancel). Use only in combination with primary or highlight buttons.
### Highlight Button
>Use highlight buttons for very high prioritized interactions as they draw a lot of attention from the user. Use as stand-alone or in combination with secondary buttons. Confirmations need to be labelled “Okay Cool”.
### Ghost Button
>Use ghost buttons for very low prioritized interactions as they draw little attention from the user. Use only in combination with other ghost buttons or primary buttons.


```jsx
import React, { Component } from 'react'

import { Button } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Button
        Big
        highlight
        icon={{
          name: 'placeholder'
          size: 24,
          color: 'black'
        }}
      />
    )
  }
}
```

```jsx
import React, { Component } from 'react'

import { GhostButton } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <GhostButton />
    )
  }
}
```
### Available Button Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`icon` | object | `{}` | Icon configuration for button with icon or button with icon on the left (name from the Merck Icon library).
`buttonStyle` | object | `{}` | Style of the Button container.
`titleStyle` | object | `{}` | Style of the Button title.
`iconLeft` | bool | `false` | Display icon on the left (available only with defined icon configuration).
`highlight` | bool | `false` | Display Highlight Button.
`secondary` | bool | `false` | Display Secondary Button.
`title` | string | `Text` | Text of the Button label.
`onPress` | func | ` ` | Function which triggers after Button pressed.
`disabled` | bool | `false` | Disable Button.
`Big` | bool | `false` | Make Button bigger.
`borderRadius` | number | `6` | Border radius of the Button.
`color` | string | `#ffffff` | Color of the title.
`fontSize` | number | `16` | Font Size of the title.
`fontFamily` | string | `Lato-Black` | Font Family of the title.
`active` | bool | `false` | Active Button state.
`width` | number | `100` | Width of the Button container.
`height` | number | `40` | Height of the Button container.


### Available Ghost Button Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`onPress` | func | `false` | Function which triggers after Ghost Button pressed.
`Big` | bool | `false` | Make Button bigger.
`title` | string | `Text` | Title of the Ghost Button.
`disabled` | bool | `false` | Disable Ghost Button.
`fontSize` | number | `14` | Font Size of the title.
`fontFamily` | string | `Lato-Regular` | Font Family of the title.
`titleStyle` | object | ` ` | Style of the title.
`active` | bool | `false` | Active state of Ghost Button.
`color` | string | `#2dbecd` | Color of the title.
`fontWeight` | number/string | `normal` | Font Weight of the title.
`titleIconWrapperStyle` | object | `{ width: 40, marginRight: 5, marginLeft: 5}` | Style of the title with icon.
`containerStyle` | object | `{ width: 70, height: 30` | Style of the Ghost Button container.
`icon` | object | `{}` | Icon configuration for Ghost Button with icon on the left or Ghost Button with icon on the right (name from the Merck Icon library).
`iconLeft` | bool | `false` | Display icon on the left.
`iconRight` | bool | `false` | Display icon on the right.
</details>

<details><summary><b>Cards</b></summary>

## **Cards**

> Cards can be used as an entrance to a category within the content or as bold links, for example on a dashboard interface. As cards have a resemblance with physical cards, it is important to stay close to the real behavior in terms of their haptics and movements.


```jsx
import React, { Component } from 'react'

import { Card } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Card />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`alignItems`        | string  | `'center'` | Align the Component on the Y axis.
`backgroundColor`   | string  | `'primary'`| Main color of the Component.
`borderRadius`      | number  | `50` | Border Radius of a Component.
`contentAlignItems`             | string  | `'center'`  |  Align the Component's content on the Y axis.
`contentHeight`          | string    | `92%`  | Height of Component's content.
`contentWidth`          | string  | `92%`      | Width of Component's content.
`height`        | number  | ``      | Height of a Component.
`width`        | number  | ``      | Width of a Component.
`info`              | bool    | `false` | Style for Component in Info state .
`justifyContent`    | string  | `'center'`| Style of Component on the X axis.
`stacked` | bool | `false` | Show a stacked Component.
</details>

<details><summary><b>Checkboxes</b></summary>

## **Checkboxes**

> Checkboxes are used to select one or multiple options from a set of available values.

```jsx
import React, { Component } from 'react'

import { Checkbox } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Checkbox checked={false} />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`disabled` | bool | `false` | Disable Checkbox.
`checked` | bool | ` ` | Determines when Checkbox is checked.
`onPress` | func | ` ` | Function called when Checkbox is pressed.
`title` | string | `Checkbox text` | Title of the Checkbox.
`fontFamily` | string | `Lato-Regular` | Font Family of the title.
`fontWeight` | number | `100` | Font Weight of the title.
`fontSize` | number | `18` | Font Size of the title.
`iconSize` | number | `30` | Size of the icon.
`titleStyle` | object | `{ color: #1b1b25 }` | Style of the title.
`iconColor` | string | `#2dbecd` | Color of The icon.
`titleContainerStyle` | object | `{}` | Style of the title container.
`iconContainerStyle` | object | `{}` | Style of the icon container.
</details>

<details><summary><b>Favorites</b></summary>

## **Favorites**

> Favorite is an icon of heart shape with animation on tap which can be used as like/unlike button.

```jsx
import React, { Component } from 'react'

import { Favorite } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Favorite
        activeColor='#f3f3f3'
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`primary` | bool | `false` | Determines when default is displayed.
`active` | bool | `false` | Determines when active is displayed.
`disabled` | bool | `false` | Determines when disabled is displayed.
`color` | string | `#e9e9ed` | Color of inactive (default) favorite.
`activeColor` | string | `#e61e50` | Color of active favorite.
`duration` | number | `525` | Duration of the favorite animation.
`onPress` | func | `() => {}` | Function called when favorite is pressed.
</details>


<details><summary><b>Dropdowns</b></summary>

## **Dropdowns**

> Dropdowns or select fields enable the user to select one option from a list of multiple options. The selection of an option can affect other form elements on the same page / screen.

```jsx
import React, { Component } from 'react'

import { Dropdown } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Dropdown
        inline
        multiSelect
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`inlineMultiselectDropdownStyle` | object | `{}` | Style of the inline, multiselect Dropdown. Valid only if 'inline' and 'multiSelect' are true.
`inlineMultiSelectContainerStyle` | object | `{} ` | Style of the inline,multiselect container. Valid only if 'inline' and 'multiSelect' are true.
`inlineDropdownContainerStyle` | object | `{}` | Style of the inline container. Valid only if 'inline' is true.
`dropdownContainerStyle` | object | `{}` | Style of the default Dropdown container.
`onOptionPress` | func | ` ` | Function called after option from list is pressed.
`inline` | bool | `false` | Determines when the Dropdown is inline.
`multiSelect` | bool | `false` | Determines when Dropdown display multiselect rows.
`dropdownLabel` | string | `Dropdown Label` | Label of the Dropdown.
`iconColor` | string | `#2dbecd` | Color of the right arrow icon.
`disabled` | bool | `false` | Determines when Dropdown is disabled.
`fontFamily` | string | `Lato-Regular` | Font Family of the Dropdown Label.
`fontSize` | number | `16` | Font Size of the Dropdown Label.
`options` | array | `{}` | Array of items for the Dropdown.
`inlineMultiselectRowStyle` | object | `{}` | Style of the Inline Multiselect Dropdown row.
`dropdownStyle` | object | `{}` | Style of the default Dropdown list.
`inlineDropdownStyle` | object | `{}` | Style of the Inline Dropdown list.
`rowStyle` | object | `{}` | Style of Default Dropdown row.
`inlineRowStyle` | object | `{}` | Style of Inline Dropdown row.
`multiSelectIconStyle` | object | `{}` | Style of the Multiselect Icon.
`labelStyle` | object | `{}` | Style of the Dropdown label.
`rowTitleStyle` | object | `{}` | Style of the row title.
`dropdownShadow` | object | `{}` | Style of Dropdown list shadow (Available only on iOS).
</details>

<details><summary><b>Filters</b></summary>

## **Filters**
> Filters are used to organize a set of available content by categories. They should always be used close to their content. Filters can be added and removed in order to decrease or increase the amount of visible content.
>
> Filters need to consist of a label and the filter icon. The selected option is displayed as a tag and replaces the label once selected.


```jsx
import React, { Component } from 'react'

import { Filter } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    const options = ["Option 1", "Option 2", "Option 3"]

    return (
      <Filter options={options} />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`disabled` | bool | `false` | Boolean for disabling the Filter.
`multiSelect` | bool | `false` | Boolean for enabling multi selection in options of the filter.
`filterPrimaryColor` | string | `#2dbecd` | Main color for filter.
`filterSecondaryColor` | string | `#f3f3f7`| Secondary color for the filter.
`filterDisabledColor` | string |`#d5d5d9`| Color for the disabled filter.
`filterContainerHeight` | number | `25` | Height of the filter container.
`filterIconSize` | number | `10` | Size of the icon size.
`filterLabel` | string | `Filter Label` | Name for a filter.
`dropdownShadow` | object | - | Style for shadow on a dropdown. **iOS only**
`options` | array | - | Array of items for the dropdown.
`dropdownPositionRight` | bool | false | Property to position dropdown to the right of the filter.
</details>

<details><summary><b>Headlines</b></summary>

## **Headlines**

> Headlines are used as an introduction into a topic and for visual differentiation between content blocks. Headlines require hierarchies and a placement conform with these.

```jsx
import React, { Component } from 'react'

import { Headline } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Headline type='H1' />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`type` | string | ` ` | type of the Headline Component. Could be: XH5 - Font Size of 40px, XH6 - Font Size of 36px, H1 -  Font Size of 32px, H2 -  Font Size of 26px, H3 -  Font Size of 22px, H4 -  Font Size of 18px, H5 -  Font Size of 16px, H6 -  Font Size of 14px, B1 -  Font Size of 48px,Merck Font Family, B2 -  Font Size of 40px,Merck Font Family, B3 -  Font Size of 36px,Merck Font Family, B4 -  Font Size of 32px,Merck Font Family, B5 -  Font Size of 26px,Merck Font Family, B6 -  Font Size of 26px,Merck Font Family
`textStyle` | object | `{}` | Style of the Headline.<br/>
`color` | string | `#1b1b25` | Color of the Headline.<br/>
`fontSize` | number | `14` | Font Size of the Headline.<br/>
`fontFamily` | string | `Lato-Black` | Font Family of the Headline.<br/>
`lineHeight` | number | `14` | Height of the line.<br/>
</details>

<details><summary><b>Icons</b></summary>

## **Icons**

> Icon is a component used to display icon from icon set.

```jsx
import React, { Component } from 'react'

import { Icon } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Icon
        name='closingX'
        size={30}
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`name` | string | `home` | Determines which icon is displayed.
`size` | number | `24` | Size of the icon.
`color` | string | `#2dbecd` | Color of the icon.
</details>

<details><summary><b>Links</b></summary>

## **Links**

> Links are hyperlinks that point at (target) any domain. There are two ways to add Link component - default and with any Paragraph (Link will display with text of the Paragraph).

```jsx
import React, { Component } from 'react'

import { Link, Paragraph } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Link
        text='Test'
        source='http://merck.design'
        active
      />
      <Paragraph
        Small
        text='Example paragraph'
        withLink
        linkText='Read more'
        source='http://merck.design'
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`text` | string | ` (isRequired)` | Link text.
`source` | string | ` (isRequired)` | Source of the link.
`color` | string | `#2dbecd` | Color of the link.
`fontSize` | number | `16` | Font size of the link.
`fontFamily` | string | `Lato Regular` | Font family of the link.
`fontWeight` | number | ` ` | Font weight of the link.
`active` | bool | `false` | Determines when active link is displayed.
`withLink` | bool | `false` | Determines when link with Paragraph is displayed.
`linkText` | string | ` (isRequired)` | Link text in Paragraph.
</details>

<details><summary><b>Lists</b></summary>

## **Lists**

> Lists are used to vertically organize content. Lists can be ordered or unordered. Unordered lists can also be used as checklists.

```jsx
import React, { Component } from 'react'

import { List, ListItem, ListHeader } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  renderRow = ({ item }) => (
    <ListItem
      title={item.title}
    />
  )

  renderHeader = () => (
    <ListHeader />
  )

  render() {
    return (
      <List
        data={data}
        renderRow={this.renderRow}
        renderHeader={this.renderHeader}
      />
    )
  }
}
```
### Available List Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`data` | array | ` ` | Array of data displayed on the List.
`renderRow` | func | ` ` | Function which renders List rows.
`renderHeader` | func | ` ` | Function which renders List header.
`listContainerStyle` | object | `{}` | Style of the List container.
`renderSeparator` | func | ` ` | Function which renders List separator.

### Available List Item Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`onPress` | func | ` ` | Function called after Item pressed.
`title` | string | ` ` | Title of the Item.
`icon` | object | ` ` | Icon configuration for Item (name from the Merck Icon library).
`iconLeft` | bool | `false` | Display Icon on the left. Valid only if icon object provided.
`disabled` | bool | `false` | Determines when Item is disabled.
`active` | bool | `false` | Determines when Item is active.
`containerStyle` | object | `{ height: 50, width: 300,alignItems: 'center' }` | Style of the Item container.
`titleStyle` | object | `{ fontSize: 14 }` | Style of the title.
`titleColor` | string | `#1b1b25` | Color of thetTitle.
`titleActiveColor` | string | `#2dbecd` | Color of the title when Item is pressed/active.
`titleActiveFontFamily` | string | `Lato-Black` | Font Family of the title when Item is pressed/active.
`titleFontFamily` | string | `Lato-Regular` | Font Family of the title.
`containerBackgroundColor` | string | `#f8f8fc` | Background Color of the container.
`containerBackgroundColorActive` | string | `#f3f3f7` | Background Color of the active container.


### Available List Header Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`onPress` | func | ` ` | Function called after Header pressed.
`bottomSeparator` | func | ` ` | Function which render bottom separator of the Header.
`title` | string | `List Head 01` | Title of the Header.
`icon` | object | ` ` | Icon configuration for Header (name from the Merck Icon library).
`iconLeft` | bool | `false` | Display Icon on the left. Valid only if icon object provided.
`containerStyle` | object, array | `{}` | Style of the Item container.
`titleStyle` | object, array | `{}` | Style of the title.
`titleFontSize` | number | `16` | Font Size of the title.
`titleFontFamily` | string | `Lato-Black` | Font Family of the title.
`titleColor` | string | `#1b1b25` | Color of the title.
`containerWidth` | number | `300` | Width of the Container.
`containerHeight` | number | `50` | Height of the Container.

</details>

<details><summary><b>Logos</b></summary>

## **Logos**

> Information on the usage of our logo can be found in the brand principles document which can be requested from our branding department at branding@merckgroup.com.

```jsx
import React, { Component } from 'react'

import { Logo } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
     <Logo/>
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`color` | string | `#503291` | Color of the Logo.
`size` | number | `90` | Size of the Logo.
`style` | object, array | `{}` | Style of the Logo.
</details>

<details><summary><b>Paragraphs</b></summary>

## **Paragraphs**

>Paragraphs are rich text blocks. The text can be formatted.

```jsx
import React, { Component } from 'react'

import { Paragraph } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Paragraph type='XLarge' />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`type` | string | ` ` | type of Paragraph component. One of: XLarge - Font Size of 22px, Large - Font Size of 18px, Medium - Font Size of 16px, Small - Font Size of 14px, XSmall - Font Size of 12px, Label - Font Size of 16px, XLabel - Font Size of 12px.
`textStyle` | object | `{}` | Style of the Paragraph.<br/>
`color` | string | `#1b1b25` | Color of the Paragraph.<br/>
`fontSize` | number | `12` | Font Size of the Paragraph.<br/>
`fontWeight` | number | `400` | Font Weight of the Paragraph.<br/>
`fontFamily` | string | `Lato-Regular` | Font Family of the Paragraph.<br/>
`letterSpacing` | number | `0` | Letter Spacing of the Paragraph.<br/>
</details>

<details><summary><b>Placeholders</b></summary>

## **Placeholders**

>Placeholders display graphic elements - available in 3 shapes (circle by default, square, rectangle).

```jsx
import React, { Component } from 'react'

import { Placeholder } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Placeholder isSquare />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`width` | number | `300` | Width of the placeholder element.
`height` | number | `300` | Height of the placeholder element.
`isSquare` | bool | `false` | If set to true will display squared placeholder.
`isRectangular` | bool | `false` | If set to true will display rectangular placeholder.
</details>

<details><summary><b>Radio Buttons</b></summary>

## **Radio Buttons**

> Radio buttons are used to select one option out of a set of multiple options.


```jsx
import React, { Component } from 'react'

import { RadioButton } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <RadioButton selected={false} />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`disabled` | bool | `false` | Disable Radio Button.
`onPress` | func | ` ` | Function called after Radio Button pressed.
`title` | string | `Radio Button Label` | Title of the Radio Button.
`fontFamily` | string | `Lato-Regular` | Font Family of the Radio Button.
`fontWeight` | number | `100` | Font Weight of the Radio Button.
`fontSize` | number | `14` | Font Size of the Radio Button.
`iconSize` | number | `24` | Size of the Radio Button Icon.
`titleStyle` | object | `{ color: #1b1b25 }` | Style of the title.
`titleContainerStyle` | object | `{}` | Style of the title container.
`iconContainerStyle` | object | `{}` | Style of the icon container.
`iconColor` | string | `#2dbecd` | Color of the Radio Button icon.
</details>

<details><summary><b>Tags</b></summary>

## **Tags**

> Tags are used for labeling items or content. Tags may also be used as a display for active filters.


```jsx
import React, { Component } from 'react'

import { Tag } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Tag />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`onPress`        | func  | `-` | OnPress Function to pass to Tag.
`text`   | string  | `'Tag Label'`| Main color of the Component.
`borderRadius`      | number  | `16` | Border Radius of a Component.
`borderWidth`             | number  | `'1.5'`  |  Width of Tag border.
`fontFamily`          | string    | `Lato-Black`  | Font family of a Tag.
`fontSize`          | number  | `12`      | Font Size of a font family.
`disabledColor`        | string  | `#e9e9ed`      | Color of disabled tag.
`outline`        | bool  | `false`      | Boolean for outline style of a Tag .
`disabled`              | bool    | `false` |Boolean for disabled style of a Tag .
`color`    | string  | `'white'`| Color of text inside Tag.
`tagColor` | bool | `false` | Color of Tag background and outline tag..
`width` | number | `false` | Width of the Tag.
`height` | number | `false` | Height of a Tag.
</details>

<details><summary><b>Text Fields</b></summary>

## **Text Fields**

> Text fields are used either in forms together with other input fields or stand-alone (e.g. value entering, newsletter etc.), for submitting data. Text field types should always be aligned with their use case (e.g. calendar, e-mail etc.). Error messages are required and should appear as quickly as possible. Error messages should describe the issue as specific as possible.


```jsx
import React, { Component } from 'react'

import { TextField } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  state={
    error: false
  }

  render() {
    return (
      <TextField
        error={this.state.error}
        multiline
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`wrapperWidth` | number | `300` | Width of the single line Text Field.
`wrapperHeight` | number | `40` | Height of the single line Text Field.
`multilineWrapperWidth` | number | `300` | Width of the multiline Text Field (Valid if multiline prop is true).
`multilineWrapperHeight` | number | `200` | Height of the multiline Text Field (Valid if multiline prop is true).
`errorMessageFontSize` | number | `12` | Font Size of the error message.
`bothTypesWrapperStyle` | obejct, array | `{ paddingLeft: 15 }` | Style for both types of Text Field Wrapper.
`errorMessageFontFamily` | string | `Lato-Regular` | Font Family of the error message.
`errorMessageColor` | string | `#e61e50` | Color of the error message.
`disabled` | bool | `false` | Disable Text Field.
`error` | bool | ` ` | Determines when error is displayed.
`errorMessage` | string | `Error Message` | Error Message.
`placeholder` | string | `Add Placeholder Text here` | Placeholder of the Text Input.
`placeholderTextColor` | string | `#a4a4ae` | Color of the placeholder.
`onFocus` | func | ` ` | Function Called when Text Field is focused.
`onBlur` | func | ` ` | Function Called when Text Field is blurred.
`errorMessageStyle` | object | `{}` | Style of the error message.
`multiline` | bool | `false` | Determines if Text Field is multiline.
`backgroundColor` | string | `transparent` | Background Color of the Text Input.
`textInputLabel` | string | `'Text Area label'` | Text of the Text Input label.
`textInputLabelColor` | string | `#a4a4ae` | Color of the Text Input label.
`textInputLabelFontFamily` | string | `Lato-Regular` | Font Family of the Text Input label.
`textInputLabelFontSize` | number | `12` | Font Size of the Text Input Label.
`textInputLabelStyle` | object | `{}` | Style of the Text Input Label.
`textInputLabelVisible` | bool | `true` | Determines if Label is visible.
`color` | string | `#1b1b25` | Color of the input text.
`fontFamily` | string | `Lato-Regular` | Font Family of the input text.
`fontSize` | number | `16` | Font Size of the input text.
`borderRadius` | number | `6` | Border Radius of the Container.
</details>

<details><summary><b>Toggles</b></summary>

## **Toggles**

> Toggles are used to select one out of two available options or to switch between two states.

```jsx
import React, { Component } from 'react'

import { Toggle } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Toggle value={true} />
    )
  }
}
```

```jsx
import React, { Component } from 'react'

import { IconToggle } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <IconToggle defaultPosition='left' />
    )
  }
}
```
### Available Toggle Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`disabled` | bool | `false` | Determines when Toggle is disabled.
`onValueChange` | func | ` ` | Function called after Toggle value changed.
`value` | bool | `false` | Determines when Toggle is true/false.
`onTintColor` | string | `#2dbecd` | Color of the active Toggle background.
`thumbTintColor` | string | `#ffffff` | Color of the Toggle Grip.
`tintColor` | string | `#f8f8fc` | Color of the Toggle background.

### Available Icon Toggle Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`onLeftState` | func | ` ` | Function Called when Toggle moves to the left side.
`onRightState` | func | ` ` | Function Called when Toggle moves to the right side.
`disabled` | bool | `false` | Determines when Toggle is disabled.
`disabledThumbTintColor` | string | `#e9e9ed` | Color of the Disabled Toggle Grip.
`disabledIconColor` | string | `#e9e9ed` | Color of the Disabled Icon.
`tintColor` | string | `#f8f8fc` | Color of the Toggle background.
`thumbTintColor` | string | `#2dbecd` | Color of the Toggle Grip.
`icon` | object | `{name: 'placeholder', size: 20, color: #ffffff}` | Icon configuration for Toggle (name from the Merck Icon library).
`defaultPosition` | string | `left` | Default Position of the Toggle left/right.
</details>

<details><summary><b>Warning Labels</b></summary>

## **Warning labels**

>Warning labels display graphic elements with warnings.

```jsx
import React, { Component } from 'react'

import { WarningLabel } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <WarningLabel name='explosive'
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`size` | number | `90` | Size of the warning element is equal to width and height.
`name` | string | ` (isRequired)` | Name of the warning element - one of: compressedGas, corrosive, environmentalHazard, explosive, flammable, harmful, healthHazard, oxidizing and toxic.
</details>

# Components

<details><summary><b>Accordions</b></summary>

## **Accordions**

> Accordion displays collapsible content panels for presenting information in a limited amount of space.

```jsx
import React, { Component } from 'react'

import { Accordion } from '@liquid-design/liquid-design-react-native'

const MULTIPLE_SECTIONS = [
  {
    title: 'Section Title',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam  eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  },
  {
    title: 'Section Title',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam  eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  }
]

class Example extends Component {
  state = {
    activeSections: []
  }

  setSections(sections) {
    this.setState({ activeSections: sections })
  }

  render() {
    return (
      <Accordion
        expandMultiple
        onChange={this.setSections.bind(this)}
        activeSections={this.state.activeSections}
        sections={MULTIPLE_SECTIONS}
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`expandMultiple` | bool | `false` | If provided, will display a multiple accordions.
`activeSections` | [number] || string | ` ` | Control which index in the sections array is currently open.
`sections` | array | `(isRequired)` | An array of sections passed to the render methods.
`initiallyActiveSection` | number | ` ` | If provided, will open chosen section on start.
`onChange` | func | ` ` | Called everytime when one of sections is clicked.
`width` | number | `335` | Width of the accordion.
`duration` | number | `200` | Duration of open/hide content animation.
`onAnimationEnd` | func | `() => {}` | Callback that is called when the animation is ended.
`disabled` | bool | `false` | Determines when disabled is displayed.
`borderColor` | string | `#e9e9ed` | The color used for the borders on top and bottom.
`titleFontFamily` | string | `Lato Black` | Font family of section title.
`titleFontSize` | number | `16` | Font size of the section title.
`titleFontWeight` | number | ` ` | Font weight of the section title.
`titleLineHeight` | number | `20` | Line height of the section title.
`inactiveTitleColor` | string | `#1b1b25` | Color of the section title when section is closed.
`activeTitleColor` | string | `#2dbecd` | Color of the section title when section is opened.
`contentFontFamily` | string | `Lato Regular` | Font family of section content.
`contentFontSize` | number | `14` | Font size of the section content.
`contentFontWeight` | number | ` ` | Font weight of the section content.
`contentLineHeight` | number | `24.5` | Line height of the section content.
`contentColor` | string | `#1b1b25` | Color of the section content.
`contentWidth` | string | `100%` | Width of the section content.
`contentHeight` | number | `125` | Height of the section content, will remove word-break effect when animating. **iOS only**
</details>

<details><summary><b>Badges</b></summary>

## **Badges**
> Badges symbolize special properties of an item or person. Badges provide a short overview about that property and its value.


```jsx
import React, { Component } from 'react'

import { Badge } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Badge withIcon />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`badgeText` | string | `Delivery in 3-4 days` | Text to display on the badge.
`withIcon` | bool | `false` | Boolean to show icon on the Badge.
`iconPosition` | string | `right` | Position of the icon.
`disabled` | bool | `false` | Style of disabled Badge.
`simpleMaxWidth` | number | `152` | Max width of Simple type Badge.
`contentBadgeWidth` | number | `300` | Width of Content type Badge.
</details>

<details><summary><b>Content Cards</b></summary>

## **Content Cards**

```jsx
import React, { Component } from 'react'

import { Badge } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <ContentCard withDetails />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`stacked` | bool | `false` | Show a stacked Component.
`imagePath` | string | ` ` | Path to Image of Content Card.
`imgWrapperSize` | number | `150` | Size of the Content Card Image.
`borderRadius` | number | `100` | Border Radius of Content Card Image.
`cardTitle` | string | `Title Name` | Title of Content Card.
`labelFirst` | string | `Label 1` | Text of a first label.
`labelSecond` | string | `Label 2` | Text of the second label.
`labelFirstValue` | string | `Value / Number ` | Value of first label.
`labelSecondValue` | string | `Value / Number ` | Value of second label.
`withDetail` | bool | `false` | Show Content Card with details.
`smallDetail` | string | `Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod` | Text of a Content Card Details.
`headingDetail` | string | `e.g. Amount, etc.` | Text of a Content Card Details Headline.
`withBadge` | bool | false | Content Card with a Badge.
</details>

<details><summary><b>Fly Outs</b></summary>

## **Fly Outs**

> Fly outs contain a limited amount of data and have the purpose of providing brief information, short forms or tables / menus. Fly outs can be opened and closed manually by the user as well as automatically or as part of another user interaction (e.g. scrolling).

```jsx
import React, { Component } from 'react'

import Flyout from '@liquid-design/liquid-design-react-native'

const options = [
  { name: 'Option 1' },
  {
    name: 'Option 2',
    options: [
      { name: 'Sub Option 1' },
      { name: 'Sub Option 2' },
      { name: 'Sub Option 3' },
      { name: 'Sub Option 4' }
    ]
  },
  { name: 'Option 3' }
]

class Example extends Component {

  render() {
    return (
      <Flyout
        options={options}
        aligned='right'
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`disabled` | bool | `false` | Determine if component is disabled.
`aligned` | string | `right` | Determined aligment of the label. One of: 'right', 'center', 'left'.
`options` | array | ` ` | Options to display on the list.
`iconSize` | number | `24` | Size of the menu icon.
`iconColor` | string | `#2dbecd` | Color of the menu icon.
`label` | string | `Flyout Label` | Label of the Flyout.
`labelFontSize` | number | `16` | Font Size of the label.
`labelColor` | string | `#1b1b25` | Color of the Label.
`labelFontFamily` | string | `Lato-Regular` | Font Family of the label.
`onLabelPress` | func | `() => {}` | Function called when label pressed.
`labelColorActive` | string | `#2dbecd` | Color of the label when is active.
`labelFontFamilyActive` | string | `Lato-Bold` | Font Family of the label when is active.
`labelStyle` | object, array | `{}` | Style of the label.
`labelContainerStyle` | object, array | `{}` | Style of the label container.
`listWidth` | number | `250` | Width of the list. Valid only in aligned: 'right' or 'left'.
`listCenteredWidth` | number | `150` | Width of the list. Valid only if aligned: 'center'.
`listBackgroundColor` | string | `#ffffff` | Background Color of the list.
`listStyle` | object, array | `{}` | Style of the list.
`headerTitle` | string | `Headline` | Header title.
`headerStyle` | object, array | `{}` | Header style.
`headerFontSize` | number | `16` | Header Font Size.
`headerFontFamily` | string | `Lato-Black` | Header Font Family.
`headerColor` | string | `#1b1b25` | Header Color.
`headerContainerStyle` | object, array | `{}` | Header Container Style.
`headerContainerHeight` | number | `59` | Header Container Height.
`onHeaderPress` | func | `() => {}` | Function Called when header is pressed.
`rowStyle` | object, array | `{}` | Row Style.
`rowHeight` | number | `53` | Height of the list row.
`rowTitleStyle` | object, array | `{}` | Row title style.
`rowTitleColor` | string | `#1b1b25` | Row title color.
`rowTitleFontSize` | number | `16` | Row title font size.
`rowTtileFontFamily` | string | `Lato-Regular` | Row title font family.
`rowBackgroundColorActive` | string | `#f3f3f7` | Background Color of the row when active.
`rowBackgroundColor` | string | `#ffffff` | Background Color of the row.
`rowTitleColorActive` | string | `#2dbecd` | Color of the row when active.
`rowTitleFontFamilyActive` | string | `Lato-Black` | Font Family of the row when active.
`onRowPress` | func | `() => {}` | Function called when row is pressed.
`arrowIconColor` | string | `#2dbecd` | Color of the arrow icon.
`arrowIconSize` | number | `24` | Size of the arrow icon.
`subOptionTitleStyle` | object, array | `{}` | Style of the suboption title.
`subOptionContainerStyle` | object, array | `{}` | Style of the suboption container.
`onSubOptionPress` | func | `() => {}` | Function called when suboption is pressed.
`subOptionContainerBackgroundColorActive` | string | `#f3f3f7` | Background of the suboption container when active.
`subOptionContainerBackgroundColor` | string | `#f8f8fc` | Background of the suboption container.
`subOptionFontFamily` | string | `Lato-Regular` | Font Family of the suboption.
`subOptionActiveFontFamily` | string | `Lato-Black` | Font Family of the suboption when active.
`subOptionActiveColor` | string | `#2dbecd` | Color of the suboption when active.
`subOptionColor` | string | `#1b1b25` | Color of the suboption.

</details>

<details><summary><b>Modals</b></summary>

## **Modals**

> Modal is a static modal example (meaning its position and display have been overridden). Component can be used in Light Box.

```jsx
import React, { Component } from 'react'

import Modal from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Modal
        withCta
        contentText='Example text'
        buttonText='Accept'
        cancelText='Cancel'
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`withGraphic` | bool | `false` | If provided, will display a different styled component - with image.
`withCta` | bool | `false` | If provided, will display a different styled component - with buttons.
`withTextField` | bool | `false` | If provided, will display a different styled component - with text field.
`headerTitle` | string | `Header Label` | Title of the header.
`headlineText` | string | `Headline Text` | Headline of the content.
`headlineType` | string | `H3` | Type of the headline.
`contentText` | string | `Lorem ipsum dolor sit amet, consetetur sadipscing elitr(...)` | Text of the content.
`buttonText` | string | `Button Text` | Title of the primary button.
`cancelText` | string | `Cancel Text` | Title of the secondary button.
`onButtonPress` | func | `() => {}` | Function called when primary button is pressed.
`onCancelPress` | func | `() => {}` | Function called when secondary button is pressed.
`onBackdropPress` | func | `() => {}` | Function called when backdrop or closing icon is pressed.
`placeholder` | string | `Add Placeholder Text here` | Text of the placeholder in modal with TextField.
`errorMessage` | string | `Error Message` | Text of the error message in modal with TextField.
`onError` | bool | `false` | Determines when error message is displayed in modal with TextField.
`onChangeText` | func | `() => {}` | Function called when text is changes in modal with TextField.
`onFocus` | func | `() => {}` | Function called when TextField is focused in modal with TextField.
`onBlur` | func | `() => {}` | Function called when TextField is blurred in modal with TextField.
`modalWidth` | number | `350` | Width of the modal.
`imagePath` | node | ` ` | Path of the image in modal with Graphic.
`contentBgColor` | string | `#ffffff` | Background color of the modal content.
`contentColor` | string | `#1b1b25` | Color of the modal content text.
`contentFontFamily` | string | `Lato Regular` | Font family of the modal content text.
`contentFontWeight` | number | ` ` | Font weight of the modal content text.
`contentFontSize` | number | `16` | Font size of the modal content text.
`contentLineHeight` | number | `28` | Line height of the modal content text.
`headerBgColor` | string | `#f8f8fc` | Backgroud color of the modal header.
`headerTitleColor` | string | `#1b1b25` | Color of the modal header title.
`headerFontFamily` | string | `Lato Regular` | Font family of the modal header title.
`headerFontWeight` | number | ` ` | Font weight of the modal header title.
`headerFontSize` | number | `12` | Font size of of the modal header title.
`headerLineHeight` | number | `15` | Line height of the modal header title.
`iconSize` | number | `20` | Size of the closing X icon.
</details>

<details><summary><b>Notifications</b></summary>

## **Notifications**

> Notifications are messages that communicate information to the user. There are 4 types of notifications: simple, reminder, error and info.

```jsx
import React, { Component } from 'react'

import { Notification } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  componentDidMount() {
   this.notificationRef.showNotification('error', 'Label error notification')
  }

 notificationRef = {}

  render() {
    return (
      <Notification
        ref={ref => this.notificationRef = ref}
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`notificationHeight` | number | `40` | Height of the notification component.
`notificationWidth` | number | `Screen width - 20` | Width of the notification component.
`type` | string | `simple` | Type of the notification - one of: simple, reminder, info, error.
`notificationLabel` | string | `Notification text` | Text of the notification label.
`closingIconColor` | string | `#ffffff` | Color of the closing icon.
`closingIconSize` | number | `20` | Size of the closing icon.
`notificationIconColor` | string | `#ffffff` | Color of the notification icon (type: reminder, error).
`notificationInfoIconColor` | string | `#1b1b25` | Color of the notification icon (type: info).
`notificationIconSize` | number | `20` | Size of the notification icon (type: reminder, error, info).
`labelColor` | string | `#ffffff` | Color of the notification label.
`labelInfoColor` | string | `#1b1b25` | Color of the notification label (type: info).
`labelFontSize` | number | `14` | Font size of the notification label.
`labelFontFamily` | string | `Lato Black` | Font family of the notification label.
`labelStyle` | object || array | `{}` | Style of the notification label.
`containerStyle` | object || array | `{}` | Style of the notification component.
`notificationRemoveTimeout` | number | `2500` | Remove timeout for notification (type: simple, reminder, info).
`onShow` | func | `() => {} ` | Function called when notification is shown.
`onDismiss` | func | `() => {} ` | Function called when notification is dismissed.
</details>

<details><summary><b>Graphs</b></summary>
  <details><summary><b>Bar Chart</b></summary>

   ## **Bar Chart**

   > Bar Chart is s a type of chart that presents labeled data with bars with heights or lengths proportional to the values that they represent. It is possible to place 8 bars in one single subset.

   ```jsx
   import React, { Component } from 'react'

   import { BarChart } from '@liquid-design/liquid-design-react-native'

   const data = [
    [
      {
        x: 2021,
        y: 150000
      },
      {
        x: 2022,
        y: 180000
      },
      {
        x: 2023,
        y: 135000
      }
    ],
    [
      {
        x: 2020,
        y: 110000
      },
      {
        x: 2021,
        y: 160000
      },
      {
        x: 2022,
        y: 180000
      },
      {
        x: 2023,
        y: 135000
      }
    ]
   ]

   class Example extends Component {
     render() {
       return (
        <BarChart
          tickValuesX={[2020, 2021, 2022, 2023]}
          tickValuesY={[0, 60000, 100000, 140000, 180000, 220000]}
          data={data}
          xAxisTickFormat={x => `${Math.round(x)}`}
          yAxisTickFormat={y => `${Math.round(y)}`}
          xTickLabelFormat={x => `${Math.round(x)}`}
          yTickLabelFormat={y => `${Math.round(y)}`}
        />
       )
     }
   }

   ```
   ### Available Props
   Prop                | Type    |  Default   | Description
   ------------------- | ------- | ---------- | -----------
   `data` | array | `(isRequired)` | Array of objects for Bar Chart. In single object - x, y, and descriptionLabel (text displayed inside the label) are required.
   `tickValuesX` | array | `(isRequired)` | Values indicated on X-axis.
   `tickValuesY` | array | `(isRequired)` | Values indicated on Y-axis.
   `unit` | string| `ml` | Unit of the values presented.
   `labels` | array| `[]` | If provided, places the labels under the graph.
   `xAxisTickFormat` | function | `` | Function that prettifies tickValues for X-axis.
   `yAxisTickFormat` | function | `` | Function that prettifies tickValues for Y-axis.
   `xTickLabelFormat` | function | `` | Function that prettifies label data value for X-axis - it is a sum of all of the bars by default.
   `yTickLabelFormat` | function | `` | Function that prettifies label data value from Y-axis.
   `barColors` | array(string) | `['#2dbecd', '#eb3c96', '#ffc832', '#503291', '#a5cd50']` | Array with colors of the bars.
   `graphHeight` | number | `360` | Height of the graph.
   `tickData` | array(string) | `` | Array with tick values from X-axis. yTickLabelFormat prop has precedence over tickData.
  </details>

  <details><summary><b>Line Graph</b></summary>

   ## **Line Graph**

   > Line graph is s a type of chart which displays information as a series of data points connected by straight line segments. By default 2 lines will be rendered, for 1 line graph use singleChart prop and for 4 lines graph use additionalCharts prop.

   ```jsx
   import React, { Component } from 'react'

   import { LineGraph } from '@liquid-design/liquid-design-react-native'

   const data = {
     primaryChart: [
       { x: 1, y: 100 },
       { x: 2.5, y: 1000 },
       { x: 3.5, y: 1500 },
       { x: 4.5, y: 2000 },
       { x: 5.5, y: 2500 },
       { x: 6.5, y: 2000 },
       { x: 7.5, y: 2000 },
       { x: 8.5, y: 1900 }
     ],
     secondaryChart: [
       { x: 1, y: 1500 },
       { x: 2.5, y: 1500 },
       { x: 3.5, y: 2200 },
       { x: 4.5, y: 2700 },
       { x: 5.5, y: 3000 },
       { x: 6.5, y: 2800 },
       { x: 7.5, y: 3000 },
       { x: 8.5, y: 2600 }
     ]
   }

   const tickValues = [0, 2000, 4000, 6000]

   class Example extends Component {
     render() {
       return (
         <LineGraph
           label1='KW 21',
           label2='KW 22',
           label3='KW 23',
           label4='KW 24',
           legendMonth='17.Oct.',
           legendYear='2017'
           tickValues={tickValues}
           primaryChart={data.primaryChart}
           secondaryChart=  {data.secondaryChart}
         />
       )
     }
   }
   ```
   ### Available Props
   Prop                | Type    |  Default   | Description
   ------------------- | ------- | ---------- | -----------
   `primaryChart` | array | ` ` | Array of values for primary line chart.
   `primaryLineColor` | string | `#2dbecd` | Color of the line for primary line chart.
   `primaryLineAnimDuration` | number | `2000` | Duration of animation for primary line chart.
   `initiallyActiveSection` | number | ` ` | If provided, will open chosen section on start.
   `secondaryChart` | array | ` ` | Array of values for secondary line chart.
   `secondaryLineColor` | string | `#ffd35b` | Color of the line for secondary line chart.
   `secondaryLineAnimDuration` | number | `2000` | Duration of animation for secondary line chart.
   `onLoadAnimDuration` | number | `1000` | Duration of animation on load charts.
   `lineSize` | number | `3` | Size of the single chart line.
   `dotSize` | number | `3.5` | Size of the dot in chart line.
   `label1` | string | ` ` | Label of the first part of description x axis.
   `label2` | string | ` ` | Label of the second part of description x axis.
   `label3` | string | ` ` | Label of the third part of description x axis.
   `label4` | string | ` ` | Label of the fourth part of description x axis.
   `legendMonth` | string | ` ` | Label of legend (day and month).
   `legendYear` | string | ` ` | Label of legend (year).
   `legendColumnWidth` | string | ` ` | Width of the legend column.
   `legendMargin` | string | `0` | Margin of the whole element (labels and legend).
   `legendWidth` | string | `300` | Width of the whole element (labels and legend).
   `legendColor` | string | `#1b1b25` | Opacity of the text in legend (labels and legend).
   `legendFontSize` | number | `12` | Font size of the text in legend.
   `tickValue` | array | ` ` | Array of the values for y axis.
   `axisOffset` | number | `50` | Offset X of the y axis.
   `graphWidth` | number | `width of the screen` | Width of the whole graph component.
   `graphWrapperPadding` | number | `50` | Left padding of the graph component.
   `unit` | string | `ml` | Unit of the values.
   `singleChart` | string | `false` | If true will render only primary line.
   `additionalChart` | string | `false` | If true will render 3 lines on graph (primary, secondary, additional1).
   `additionalCharts` | string | `false` | If true will render 4 lines on graph (primary, secondary, additional1, additional2).
   `additionalChart1` | array | ` ` | Array of values for first additional line chart.
   `additionalColor1` | string | `#000000` | Color of the line for first additional line chart.
   `additionalLineAnimDuration` | number | `2000` |  Duration of animation for first additional line chart.
   `additionalChart2` | array | ` ` | Array of values for second additional line chart.
   `additionalColor2` | string | `#000000` | Color of the line for second additional line chart.
   `additionalLineAnimDuration2` | number | `2000` |  Duration of animation for second additional line chart.
  </details>
</details>

<details><summary><b>Paginations</b></summary>

## **Paginations**

> Pagination is component which enables to make carousel of views with pagination.

```jsx
import React, { Component } from 'react'
import { Pagination } from '@liquid-design/liquid-design-react-native'

class Example extends Component {
state = {
items: [{
  title: 'Merck Design'
},
{
  title: 'Liquid Design System'
},
{
  title: 'Product Design leaves space for the brand.'
},
{
  title: 'Product Design is unobstrusive.'
},
{
  title: 'Color sits at the heart of our brand identity.'
}]
}

renderItem = ({ item }) => (
  <View>
    <Text>{item.title}</Text>
  </View>
)

  render() {

    return (

      <Pagination
        refProp={(c) => { this.carouselRef = c }}
        carouselRef={this.carouselRef}
        data={this.state.items}
        renderItem={this.renderItem}
        onSnapToItem={index => this.setState({ activeSlide: index })}
        dotsLength={this.state.items.length}
        activeDotIndex={this.state.activeSlide}
       />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`data` | array | `(isRequired)` | Array of carousel views.
`carouselRef` | object | `(isRequired)` | Control which views are in pair with pagination.
`renderItem` | func | `(isRequired)` | Render paginated views.
`dotsLength` | number | `(isRequired)` | Length of the data with carousel views.
`refProp` | func | `(isRequired)` | Ref of the whole component.
`innerWidth` | number | `210` | Width of the space where numbers in pagination are displayed.
`paginationWidth` | string | `100%` | Width of the whole pagination.
`paginationHeight` | number | `100` | Height of the whole pagination.
`sliderHeight` | number | `500` | Height of the views which are in carousel.
`iconPaddingLeft` | number | `10` | Padding left of the arrow icon.
`iconPaddingRight` | number | `10` | Padding right of the arrow icon.
`iconSize` | number | `20` | Size of the arrows icons.
`itemWidth` | number | `24` | Width of the element in pagination (number).
`itemHeight` | number | `24` | Height of the element in pagination (number).
`itemPaddingLeft` | number | `8` | Padding left of the element in pagination (number).
`itemPaddingRight` | number | `8` | Padding right of the element in pagination (number).
`fontFamily` | string | `Lato Regular` | Font family of the element in pagination (number).
`fontSize` | number | `14` | Font size of the element in pagination (number).
`lineHeight` | number | `24.5` | Line height of the element in pagination (number).
`activeFontWeight` | number | ` ` | Font weight of the active element in pagination (number).
`activeTextColor` | string | `#2dbecd` | Color of active element in pagination (number).
`inactiveTextColor` | string | `#1b1b25` | Color of inactive element in pagination (number).
`activeBackgroundColor` | string | `#2dbecd` | Background color of active element in pagination (number).
`disabled` | bool | `false` | Specify when component is disabled.
</details>

<details><summary><b>Progress Bars</b></summary>

## **Progress Bars**

> Progress bars indicate the advance of a certain interaction or process. Common use cases are page scrolling or processes like check out or registration.<br>
### **Linear Progress Bar**
> Use linear progress bars for summarized overviews of a certain progress, as deliveries or timers.<br>
```jsx
import React, { Component } from 'react'

import ProgressBar from '@liquid-design/liquid-design-react-native'

class Example extends Component {
    state = {
      progress: 0
  }

  handleAdd = () => {
    const { progress } = this.state

    const step = 5
    const maxVal = 200

    if (progress < maxVal) {
      this.setState({ progress: progress + step })
    }
  }

  handleRemove = () => {
    const { progress } = this.state

    const step = 5
    const minVal = 0
    if (progress > minVal) {
      this.setState({ progress: progress - step })
    }
  }

  render() {
    return (
        <ProgressBar
          progress={this.state.progress}
          type='linear'
        />
      )
    }
  }
```
### **Linear Progress Bar**
> Use circular progress bars for quickly advancing processes, preferrably animated, as installations or scales.
```jsx
import React, { Component } from 'react'

import ProgressBar from '@liquid-design/liquid-design-react-native'

class Example extends Component {
    state = {
      progress: 0
  }

  handleAdd = () => {
    const { progress } = this.state

    const step = 5
    const maxVal = 200

    if (progress < maxVal) {
      this.setState({ progress: progress + step })
    }
  }

  handleRemove = () => {
    const { progress } = this.state

    const step = 5
    const minVal = 0
    if (progress > minVal) {
      this.setState({ progress: progress - step })
    }
  }

  render() {
    return (
        <ProgressBar
          progress={this.state.progress}
          type='circle'
        />
      )
    }
  }
```


### **Step Progress Bar**
> Use the step progress bar for processes as setups, checkouts or registrations.<br>

```jsx
import React, { Component } from 'react'

import ProgressBar from '@liquid-design/liquid-design-react-native'

class Example extends Component {
    state = {
    currentStep: 0,
    labels: ['Step Label', 'Step Label', 'Step Label', 'Step Label']
  }

  handleAddStep = () => {
    const { currentStep, labels } = this.state

    const step = 1
    const maxVal = labels.length
    if (currentStep < maxVal) {
      this.setState({ currentStep: currentStep + step })
    }
  }

  handleRemoveStep = () => {
    const { currentStep } = this.state

    const step = 1
    const minVal = 0
    if (currentStep > minVal) {
      this.setState({ currentStep: currentStep - step })
    }
  }

  render() {
    const { labels } = this.state
    return (
       <ProgressBar
          currentPosition={currentStep}
          type='step'
          labels={labels}
      />
      )
    }
  }
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`inactive` | bool | `false` | Disables Progress Bar.
`type` | string | `circle` | Type of Progress Bar to render.
`progress` | number | ` ` | Value of Progress in Circle and Linear Progress Bar (see example).
`linearProgressBarWidth` | number | `300` | Width of a Linear Progress Bar.
`circleSize` | number | `140` | Size of a Circle.
`circleLabel` | string | `Label` | Label inside a Circle.
`currentPosition` | number | ` ` Prop for manipulating current position of the Step Progress Bar (see example).
`stepCount` | `4` | Number of steps in Step Progress Bar.
`labels` | array | ` ` | Labels for Step Indicators of Step Progress Bar.
`disabledStep` | oneOf([arrayOf(number) number]) |
`primaryProgressBarColor` | string | `colors.vibrantGreenDefault` | Color for default Linear and Circle Progress Bar.
`overdueProgressBarColor` | string |  `colors.richRedDefault` | Color for overdue Linear and Circle Progress Bar.
`overdueProgressBarUnfilledColor` | string |  `colors.richRedLightest` | Color for unfilled overdue  for Linear and Circle Progress Bar.
`inactiveProgressBarColor` | string | `colors.sensitiveGreyDarker` | Color for inactive Linear and Circle Progress Bar.
`inactiveProgressBarUnfilledColor` | string | `colors.sensitiveGreyDefault` | Color for inactive Linear and Circle Progress Bar.
`customStyle` |object| `see below` |  Object for Custom Styles of a Step Progress Bar.

```js
const customStyle = {
  stepIndicatorSize: 24,
  currentStepIndicatorSize: 24,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 0,

  primaryStepProgressBarColor: colors.vibrantCyanDefault,
  unfinishedStepProgressBarColor: colors.sensitiveGreyDarkest,
  disabledStepProgressBarColor: colors.sensitiveGreyDark,

  stepIndicatorFinishedBackgroundColor: colors.white,
  stepIndicatorUnFinishedBackgroundColor: colors.white,
  stepIndicatorCurrentBackgroundColor: colors.white,
  stepIndicatorLabelFontSize: 12,

  labelStepProgressBarColor: colors.richBlackDefault,
  labelSize: 12,
  currentStepIndicatorLabelFontSize: 12,
  currentStepLabelColor: labelStepProgressBarColor,
  finishedStepLabelColor: labelStepProgressBarColor,
  unfinishedStepLabelColor: colors.richBlackLight
}
```
</details>

<details><summary><b>Quotes</b></summary>

## **Quotes**

> Quotes are used to display quotation and author. When imagePath is provided, will display a differently styled quote. Default is with small text of quotation.

```jsx
import React, { Component } from 'react'

import { Quote } from '@liquid-design/liquid-design-react-native'

const quotation = 'Digital design is like painting, exept the paint never dries.'

class Example extends Component {

  render() {
    return (
      <Quote
        quotation={quotation}
        author='Author'
        big
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`quotation` | string | ` (isRequired)` | Quotation text.
`author` | string | ` (isRequired)` | Author of quotation.
`big` | bool | `false` | Determines when big text is displayed.
`small` | bool | `true` | Determines when small text is displayed.
`imagePath` | object | ` ` | If provided, will display a differently styled quote - with image.
`borderRadius` | number | `100` | Border radius of the image.
`width` | number | `250` | Width of the whole component.
`fontFamily` | string | `Lato Black` | Font family of the quotation text.
`fontSize` | number | `22` | Font size of the small quotation text (for big is 32).
`fontWeight` | number | ` ` | Font weight of the quotation text.
`lineHeight` | number | `27.5` | Line height of the small quotation text (for big is 40).
`color` | string | `#1b1b25` | Color of the quotation text.
`authorFontFamily` | string | `Lato Regular` | Font family of the author text.
`authorFontSize` | number | `12` | Font size of the author text.
`authorFontWeight` | number | ` ` | Font weight of the author text.
`authorLineHeight` | number | `15` | Line height of the author text.
`authorColor` | string | `#2dbecd` | Color of the author text.
</details>

<details><summary><b>Ratings</b></summary>

## **Ratings**

> Ratings provide an estimated value on the quality of a certain element. They can be interactive for the user.


```jsx
import React, { Component } from 'react'

import { Rating } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Rating maxIcons={5} />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`disabled` | bool | `false` | Disable Rating.
`maxIcons` | number | ` ` | Determines amount of icons dispalyed.
`dot` | bool | `false` | Determines to display dots instead of Stars.
`activeIconColor` | string | `#2dbecd` | Color of the Active icon.
`inactiveIconColor` | string | `#e9e9ed` | Color of the Inactive icon.
`iconSize` | number | `24` | Size of the icon.
`rating` | number | `0` | Rating Amount.
`onIconPress` | func | ` ` | Function called after icon pressed.
</details>

<details><summary><b>Search Bars</b></summary>

## **Search Bars**

> Search bars enable the user to enter a search term or phrase which will then be processed by the search engine.

> In order to make this component work, You have to wrap your container in ScrollView and add 'keyboardShouldPersistTaps='handled'' prop.


```jsx
import React, { Component } from 'react'
import { ScrollView, View } from 'react-native'
import { SearchBar } from '@liquid-design/liquid-design-react-native'

const searchOptions = [
  { text: 'Search Result 1' },
  { text: 'Search Result 2' },
  { text: 'Search Result 3' },
  { text: 'Search Result 4' },
  { text: 'Search Result 5' },
  { text: 'Search Result 6' },
  { text: 'Search Result 7' }
]

class Example extends Component {

  render() {
    return (
      <ScrollView
        contentContainerStyle={{ flex: 1 }}
        scrollEnabled={false}
        keyboardShouldPersistTaps='handled'
      >
        <View>
          <SearchBar
            searchOptions={searchOptions}
            searchProperty='text'
            keyExtractor={item => item.text}
          />
        </View>
      </ScrollView>
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`width` | number | `250` | Width of the Search Bar Container with Text Input.
`height` | number | `50` | Height of the Search Bar Container with Text Input.
`backgroundColor` | string | `#f8f8fc` |  Background Color of the Search Bar Container with Text Input.
`iconSize` | number | `24` | Size of the Icon.
`iconColor` | string | `#e9e9ed` | Color of the Icon.
`iconColorActive` | string | `#2dbecd` | Color of the Icon when active.
`placeholder` | string | `Search...` | Placeholder of the Text Input.
`placeholderTextColor` | string | `#a4a4ae` | Color of the Placeholder.
`inputTextColor` | string | `#1b1b25` | Color of the input Text.
`inputTextFontFamily` | string | `Lato-Regular` | Font Family of the input Text.
`inputTextFontSize` | number | `16` | Font size of the input Text.
`onFocus` | func | `() => {}` | Function called when Text Input is focused.
`onBlur` | func | `() => {}` | Function called when Text Input is blurred.
`onChangeText` | func | `() => {}` | Function called when Text Input value change.
`searchOptions` | array | ` ` | Array of the options to Search.
`keyExtractor` | func | `() => {}` | Custom key extractor for the list.
`searchProperty` | string | ` ` | Property which specify by which property search array.
`ItemSeparatorComponent` | func | `() => {}` | Separator of the list.
`rowStyle` | object | `{}` | Style of the each row.
`rowTitleStyle` | object | `{}` | Style of the row title.
`onRowPress` | func | `() => {}` | Function called after row pressed.
`listContainerStyle` | object | `{}` | Style of the list container.
`searchableRange` | number | `4` | Length of the list.
`disabled` | bool | `false` | Specify when component is disabled.
`ghost` | bool | `false` | Specify transparency of Text Input container.
`containerBackgroundColorActive` | string | `#f3f3f7` | Color of the row background when pressed.
`containerBackgroundColor` | string | `#f8f8fc` | Color of the row background.
`titleFontFamily` | string | `Lato-Regular` | Font Family of the row title.
`titleActiveFontFamily` | string | `Lato-Black` | Font Family of the acitve row title.
`titleActiveColor` | string | `#2dbecd` | Color of the active row title.
`titleColor` | string | `#1b1b25` | Color of the row title.
`borderBottomColor` | string | `#2dbecd` | Color of the Container bottom border.
`listBackgroundColor` | string | `#f8f8fc` | Background color of the list container.

</details>

<details><summary><b>Sliders</b></summary>

## **Sliders**

> Slider is a component used to select a single value from a range of values.

```jsx
import React, { Component } from 'react'

import { Slider } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Slider
        minimumValue={10}
        maximumValue={60}
        step={10}
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`icon` | bool | `false` | If provided, will display a differently styled slider - with icons.
`disabled` | bool | `false` | Determines when disabled is displayed.
`minimumValue` | number | `0` | Initial minimum value of the slider.
`maximumValue` | number | `100` | Initial maximum value of the slider.
`step` | number | `1` | Step value of the slider. The value should be between 0 and (maximumValue - minimumValue).
`onSlidingStart` | func | ` ` | Callback that is called when the user starts dragging the slider, regardless if the value has changed.
`onSlidingComplete` | func | ` ` | Callback that is called when the user releases the slider, regardless if the value has changed.
`minimumTrackTintColor` | string | `#2dbecd` | The color used for the track to the left of the button.
`maximumTrackTintColor` | string | `#e9e9ed` | The color used for the track to the right of the button.
`thumbTintColor` | string | `#ffffff` | Background color of the thumb.
`thumbStyle` | object | `{ borderWidth: 1, borderColor: #e9e9ed }` | By default provided for Android. If provided, will override thumb style on iOS and Android.
`trackStyle` | object | ` ` | If provided, will assign style for the track.
`valueFontFamily` | string | `Lato Black` | Font family of the label above thumb.
`valueFontSize` | number | `18` | Font size of the label above thumb.
`valueFontWeight` | number | ` ` | Font weight of the label above thumb.
`valueLineHeight` | number | ` ` | Line height of the label above thumb.
`valueColor` | string | `#2dbecd` | Color of the label above thumb.
`labelFontFamily` | string | `Lato Regular` | Font family of the label on the top of slider.
`labelFontSize` | number | `12` | Font size of the label on the top of slider.
`labelFontWeight` | number | ` ` | Font weight of the label on the top of slider.
`labelLineHeight` | number | `15` | Line height of the label on the top of slider.
`labelColor` | string | `#1b1b25` | Color of the label on the top of slider.
`labelStyle` | object | `{ marginBottom: 25, marginLeft: 20 }` | If provided, will override styles for label on Android and iOS.
`fontFamily` | string | `Lato Regular` | Font family of the label on left and right of slider.
`fontSize` | number | `12` | Font size of the label on left and right of slider.
`fontWeight` | number | ` ` | Font weight of the label on left and right of slider.
`lineHeight` | number | `15` | Line height of the label on left and right of slider.
`color` | string | `#1b1b25` | Color of the label on left and right of slider.
`iconSize` | number | `24` | Size of the icons on the left and right.
`sliderWidth` | number | `250` | Width of the slider (track with thumb).
</details>

<details><summary><b>Social Shares</b></summary>

## **Social Shares**

> Social shares contain options to share a specific content on social networks or can be used as links to related accounts within the social network / platforms.


```jsx
import React, { Component } from 'react'

import { SocialShare } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  state={
    error: false
  }

  render() {
    return (
      <SocialShare
        type='facebook'
        large
      />
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`type` | string | ` ` | Type of Social Share button. One of: facebook, instagram, snapchat, twitter, linkedIn, flickr, slack, mail, salesforce, skype, microsoftTeams, xing.
`width` | number | `40` | Width of the small button. <br />
`height` | number | `40` | Height od the small/large button.<br />
`large` | bool | `false` | Defines if button is large. <br />
`fontFamily` | string | `Lato-Bold` | Font Family of the large button label.<br />
`fontSize` | number | `16` | Font Size of the large button label.<br />
`color` | string | `#ffffff` | Color of the large button label.<br />
`onPress` | func | ` ` | Function Called when button is pressed.<br />
`borderRadius` | number | `6` | Border Radius value of the button container.<br />
`largeWidth` | number | `160` | Width of the large button.<br />
`iconSize` | number | `24` | Size of the icon.<br />
`containerStyle` | object | `{}` | Style of the button container.<br />
`labelStyle` | object | `{}` | Style of the large button label.<br />
</details>

<details><summary><b>Tables</b></summary>

## **Tables**

> Tables are used to put content into a ordered grid. The first line is always the header line and defines the content below for each column. Each column can be sorted.

> In order to make all the Tables work, You have to structurize your data as tableData array

> In order to make Checkbox Table work and get all the features, You have to pass following props: checkbox, mainCheckboxChecked, onMainCheckboxPress, onCheckboxPress

> In order to make Dropdown Table work and get all the features, You have to pass following props: dropdown, mainOptionOpened, onMainOptionOpen, onOptionOpened

> In order to make Table with pagination work just pass 'withPagination' and 'paginationItemsPerPage' prop to Table. also Table takes all the props from TablePagination component


```jsx
const dropdownData = 'We\'ve been around for 350 years, yet our majority owners are still the descendants of Friedrich Jacob Merck,the man who founded our company in Darmstadt, Germany in 1668. Since then, we have become a truly global company with 52,000 employees in 66 countries working on breakthrough solutions and technologies. '

const rowsData= [
  {
    rowName: 'Name 02',
    rowLabel: 'Label 02',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2'],
    rowDataLabel: ['Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2', 'Lorem Ipsum dolor2'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    dropdownInfoOpened: true,
    checked: true,
    disabled: false
  },
  {
    rowName: 'Name 01',
    rowLabel: 'Label 01',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1'],
    rowDataLabel: ['Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1', 'Lorem Ipsum dolor1'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    dropdownInfoOpened: false,
    checked: false,
    disabled: true
  },
  {
    rowName: 'Name 04',
    rowLabel: 'Label 04',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4'],
    rowDataLabel: ['Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4', 'Lorem Ipsum dolor4'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    checked: true,
    dropdownInfoOpened: true,
    disabled: false
  },
  {
    rowName: 'Name 03',
    rowLabel: 'Label 03',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3'],
    rowDataLabel: ['Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3', 'Lorem Ipsum dolor3'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    checked: false,
    dropdownInfoOpened: false,
    disabled: false
  },
  {
    rowName: 'Name 06',
    rowLabel: 'Label 06',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6'],
    rowDataLabel: ['Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6', 'Lorem Ipsum dolor6'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    checked: true,
    dropdownInfoOpened: true,
    disabled: false
  },
  {
    rowName: 'Name 05',
    rowLabel: 'Label 05',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5'],
    rowDataLabel: ['Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5', 'Lorem Ipsum dolor5'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    checked: false,
    dropdownInfoOpened: false,
    disabled: false
  },
  {
    rowName: 'Name 08',
    rowLabel: 'Label 08',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8'],
    rowDataLabel: ['Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8', 'Lorem Ipsum dolor8'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    checked: false,
    dropdownInfoOpened: false,
    disabled: false
  },
  {
    rowName: 'Name 07',
    rowLabel: 'Label 07',
    rowInfo: 'Info',
    rowData: ['Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7'],
    rowDataLabel: ['Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7', 'Lorem Ipsum dolor7'],
    dropdownInfo: dropdownData,
    imageUrl: yourUrl,
    checked: false,
    dropdownInfoOpened: false,
    disabled: false
  }
]

const headersData = ['Column 01', 'Column 02', 'Column 03', 'Column 04', 'Column 05', 'Column 06', 'Column 07', 'Column 08']
```
## Default Table

```jsx
import React, { Component } from 'react'

import { Table } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Table
        type='small'
        rowsData={rowsData}
        headersData={headersData}
      />
    )
  }
}
```

## Checkbox Table

```jsx
import React, { Component } from 'react'

import { Table } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  state={
    mainCheckboxChecked: false
  }

  render() {
    return (
      <Table
        type='small'
        rowsData={rowsData}
        headersData={headersData}
        checkbox
        mainCheckboxChecked={this.state.mainCheckboxChecked}
        onMainCheckboxPress={() => {
          this.setState({
            mainCheckboxChecked: !this.state.mainCheckboxChecked
          })
        }}
        onCheckboxPress={(value, index, isEveryChecked) => {
          this.setState({
            mainCheckboxChecked: isEveryChecked
          })
        }}
      />
    )
  }
}
```

## Dropdown Table

```jsx
import React, { Component } from 'react'

import { Table } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  state={
    mainOptionOpened: false
  }

  render() {
    return (
      <Table
        type='small'
        rowsData={rowsData}
        headersData={headersData}
        dropdown
        mainOptionOpened={this.state.mainOptionOpened}
        onMainOptionOpen={() => {
          this.setState({
            mainOptionOpened: !this.state.mainOptionOpened
          })
        }}
        onOptionOpened={(value, index, isEveryOpened) => {
          this.setState({
            mainOptionOpened: isEveryOpened
          })
        }}
      />
    )
  }
}
```

## Image Table

```jsx
import React, { Component } from 'react'

import { Table } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Table
        type='image'
        tableData={tableData}
      />
    )
  }
}

```

## Table with pagination

```jsx
import React, { Component } from 'react'

import { Table } from '@liquid-design/liquid-design-react-native'

const itemsPerPage = [10, 20, 40, 80]

class Example extends Component {

  render() {
    return (
      <Table
        type='image'
        tableData={tableData}
        withPagination
        paginationItemsPerPage={itemsPerPage}
      />
    )
  }
}

```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`type` | string | `small` | Type of the Table one of: small, medium, large, image.
`rowsData` | array | `[]` | Rows Data that need to be passed to Table component. It have to be structurized like above.
`headersData` | array | `[]` | Headers Data that need to be passed to Table component. It have to be structurized like above.
`cellWidth` | number | `130` | Width of every cell from the Table.
`cellStyle` | array, object | `{}` | Style of every cell from the Table.
`scrollAreaWidth` | number | `300` | Width of the scrolling area.
`scrollAreaHeight` | number | `300` | Height of the scrolling area.
`spaceBetweenColumns` | number | `30` | Space between columns.
`nameTextFontFamily` | string | `Lato-Black` | Font family of the names from first column.
`nameTextFontSize` | number | `14` | Font size of the names from first column.
`nameTextColor` | string | `#1b1b25` | Color of the names from first column.
`nameTextStyle` | object, array | `{}` | Style of the names from first column.
`infoTextFontFamily` | string | `Lato-Regular` | Font family of the info from first column.
`infoTextFontSize` | number | `14` | Font size of the info from first column.
`infoTextStyle` | object, array | `{}` | Style of the info from first column.
`headerWidth` | number | `130` | Width of the headers.
`headerWrapperStyle` | object, array | `{}` | Style of the headers wrappers
`headerBackgroundColor` | string | `#f8f8fc` | Background color of the headers.
`headerTextStyle` | object, array | `{}` | Style of the headers.
`headerTextFontFamily` | string | `Lato-Black` | Font family of the headers.
`headerTextFontSize` | number | `14` | Font size of the headers.
`headerTextColor` | string | `` | Color of the headers.
`rowTextFontFamily` | string | `Lato-Regular` | Font family of the row text.
`rowTextFontSize` | number | `14` | Font size of the row text.
`rowTextColor` | string | `#1b1b25` | Color of the row text.
`rowBackgroundColor` | string | `#ffffff` | Background color of the rows.
`rowTextStyle` | object, array | `{}` | Style of the row text.
`arrowIconColor` | string | `#2dbecd` | Color of the arrow icons.
`arrowIconSize` | number | `24` | Size of the arrow icons.
`tableDataBackgroundColor` | string | `#ffffff` | Background color of the table data.
`checkbox` | bool | `false` | Specifies if table has checkboxes. Required checkbox to work.
`mainCheckboxChecked` | bool | `false` | Specifies if table checkboxes are checked. Required checkbox to work.
`onMainCheckboxPress` | func | `() => {}` | Function that triggers when main checkbox pressed.  Required checkbox to work.
`onCheckboxPress` | func | `() => {}` | Function that triggers when checkbox pressed.  Required checkbox to work.
`checkboxIconSize` | number | `19` | Size of the checkbox icons.
`checkboxIconColor` | string | `#2dbecd` | Color of the checkbox icons.
`dropdown` | bool | `false` | Specifies if table has dropdowns. Required dropdown to work.
`mainOptionOpened` | bool | `false` | Specifies if table dropdowns are opened. Required dropdown to work.
`onMainOptionOpen` | func | `() => {}` | Function that triggers when main dropdown pressed. Required dropdown to work.
`onOptionOpened` | func | `() => {}` | Function that triggers when dropdown pressed. Required dropdown to work.
`imageWidth` | number | `40` | Width of the image.
`imageHeight` | number | `40` | Height of the image.
`imageResizeMode` | string | `contain` | Resize mode of the image.
`imageBorderRadius` | number | `20` | Border Radius of the image.
`dropdownInfoTextFontSize` | number | `14` | Font Size of the dropdown info.
`dropdownInfoTextFontFamily` | string | `Lato-Regular` | Font Family of the dropdown info.
`dropdownInfoTextColor` | string | `#1b1b25` | Color of the dropdown info.
`labelTextFontSize` | number | `12` | Font size of the labels. Available only for large tables.
`labelTextFontFamily` | string | `Lato-Regular` | Font family of the labels. Available only for large tables.
`labelTextColor` | string | `#1b1b25` | Color of the labels. Available only for large tables.
`labelTextStyle` | object, array | `{}` | Style of the labels. Available only for large tables.
`withPagination` | bool | `false` | Specifies if table pagination component is rendered.
`paginationBelow` | bool | `false` | Specifies if table apgination component is rendered below.
`paginationItemsPerPage` | array | `[10, 20, 40, 80]` | Items per page array rendered in dropdown.
`actualItemsPerPage` | number | `10` | Actual number of items visible
`currentPage` | number | `1` | Actual number of page.
`onItemsDropdownRowPressed` | func | `` | Function triggered after items per page dropdown row pressed.
`onPagesDropdownRowPressed` | func | `` | Function triggered after page dropdown row pressed.
`wrapperColor` | string | `#ffffff` | Default wrapper background color.
`onSortOrderChanged` | func | undefined | Function that allows to apply the custom sorting of the data. It can receive 3 arguments - columnSortedAscending (bool), columnUnsorted (bool), index (int - current index of the column).

</details>

<details><summary><b>Table Pagination</b></summary>

## **Table Pagination**

> Use the table pagination for long tables that need to be spread across multiple pages.


```jsx
import React, { Component } from 'react'

import { TablePagination } from '@liquid-design/liquid-design-react-native'

const itemsPerPage = [10, 20, 40, 80]
class Example extends Component {

  render() {
    return (
      <TablePagination
        itemsPerPage={itemsPerPage}
      />
    )
  }
}
```
### Avaliable Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`width` | number | `600` | Width of the pagination component, with table component it takes 100% width of the table.
`height` | number | `32` | Height of the pagination component.
`itemsPerPage` | array | `[]` | Array with numers which specifies items per page.
`backgroundColor` | string | `#ffffff` | Background color of the pagination component.
`itemsPerPageLabel` | string | `Items per page: ` | Label on far left side of the component
`itemsPerPageLabelStyle` | array, object | `{}` | Style of the far left side label.
`itemsPerPageLabelFontFamily` | string | `Lato-Regular` | Font family of the far left side label
`itemsPerPageLabelFontSize` | number | `12` | Font size of the far left side label.
`itemsPerPageLabelColor` | string | `#1b1b25` | Color of the far left side label.
`itemsPerPageNumberFontSize` | number | `14` | Font size of the choosen item per page.
`itemsPerPageNumberColor` | string | `#2dbecd` | Color of the choosen item per page.
`itemsPerPageNumberStyle` | array, object | `{}` | Style of the choosen item per page.
`iconSize` | number | `20` | Size of the arrow icon on the left and on the right
`iconColor` | string | `#2dbecd` | Color of the arrow icon on the left and on the right
`amountAndRangeSplittingLabel` | string | `of` | String which slit amount of items and range of pages.
`itemsRangeFontSize` | number | `12` | Font size of the items range label.
`itemsRangeFontFamily` | string | `Lato-Regular` | Font family of the items range label.
`itemsRangeColor` | string | `#1b1b25` | Color og the items range label.
`itemsRangeStyle` | array, object | `{}` | Style of the items range label
`pagesRangeFontSize` | number | `12` | Font Family of the pages range label.
`pagesRangeFontFamily` | string | `Lato-Regular` | Font Family of the pages range label.
`pagesRangeColor` | string | `#1b1b25` | Color of the pages range label.
`pagesRangeStyle` | object, array | `{}` | Style of the pages range label.
`horizontalIconSize` | number | `24` | Size of the Left/Right arrow icon.
`horizontalIconColor` | string | `#2dbecd` | Color of the Left/Right arrow icon.
`pageNumberFontSize` | number | `14` | Font size of the actual page number.
`pageNumberFontFamily` | string | `Lato-Black` | Font Family of the actual page number.
`pageNumberColor` | string | `#2dbecd ` | Color of the actual page number.
`pageNumberStyle` | object, array | `{}` | Style of the actual page number.
`itemsDropdownWidth` | number | `145` | Width of the items per page dropdown.
`itemsDropdownBackgroundColor` | string | `#ffffff` | Background color of the items per page dropdown.
`itemsDropdownStyle` | array, object | `{}` | Style of the items per page dropdown.
`dropdownItemsRowBackroundColorActive` | string | `#f3f3f7` | Background color of the items per page dropdown row when pressed.
`dropdownItemsRowBackroundColor` | string | `#ffffff` | Background color of  the items per page dropdown row when pressed.
`dropdownItemsRowTextFontFamily` | string | `Lato-Regular` | Font family of the items per page dropdown row label.
`dropdownItemsRowTextFontFamilyActive` | string | `Lato-Black` | Font family of the items per page dropdown row label when pressed.
`dropdownItemsRowTextColorActive` | string | `#2dbecd` | Color of the items per page dropdown row label when pressed.
`dropdownItemsRowTextColor` | string | `#1b1b25` | Color of the items per page dropdown row label.
`dropdownItemsRowWidth` | number | `145` | Width of the items per page dropdown row.
`dropdownItemsRowHeight` | number | `40` | Height of the items per page dropdown row.
`dropdownItemsRowTextFontSize` | number | `14` | Font size of the
`onItemsDropdownRowPressed` | func | `` | Function triggered after items per page dropdown row pressed.
`amountItemsLabel` | string | `items` | String which describe amount of items. Placed on the far left.
`amountPagesLabel` | string | `pages` | String which describe amount of pages. Placed on the far right.
`itemsAmount` | number | `100` | Number of items passed to component/ Number of records passed from array with data.
`paginationBelow` | bool | `false` | Specifies if pagination component should be rendered below the table.
`actualItemsPerPage` | number | `10` | Actual number of items visible.
`pagesDropdownWidth` | number | `145` | Width of the pages dropdown.
`pagesDropdownBackgroundColor` | string | `#ffffff ` | Background color of the pages dropdown.
`pagesDropdownStyle` | object, array | `{}` | Style of the pages dropdown.
`dropdownPagesRowBackroundColorActive` | string | `#f3f3f7` | Background color of the pages dropdown row when pressed.
`dropdownPagesRowBackroundColor` | string | `#ffffff` | Background color of the pages dropdown row.
`dropdownPagesRowTextFontFamily` | string | `Lato-Regular` | Font Family of the pages dropdown row label.
`dropdownPagesRowTextFontFamilyActive` | string | `Lato-Black` | Font family of the pages dropdown row label when pressed.
`dropdownPagesRowTextColorActive` | string | `#2dbecd` | Color of the pages dropdown row label when pressed.
`dropdownPagesRowTextColor` | string | `#1b1b25` | Color of the pages dropdown row label.
`dropdownPagesRowWidth` | number | `145` | Width of the pages dropdown row.
`dropdownPagesRowTextFontSize` | number | `14` | Font size of the pages row label.
`currentPage` | number | `1` | Number of current visible page.
`onPagesDropdownRowPressed` | func | `` | Function triggered after page dropdown row pressed.
`pagesWrapperStyle` | object, array | `{}` | Style of the pages content wrapper.
`itemsPerPageWrapperStyle` | object, array | `{ paddingLeft: 20 }` | Style of the items content wrapper.
`wrapperColor` | string | `#ffffff` | Default wrapper background color.

</details>

<details><summary><b>Tooltips</b></summary>

## **Tooltips**

> Tooltips provide additional information, mostly short paragraphs, and can be placed besides all sorts of interface elements.


```jsx
import React, { Component } from 'react'

import Tooltip from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Tooltip
        modalRenderSide='bottom-left'
      />
    )
  }
}
```
### Avaliable Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`modalRenderSide` | string | `bottom-left` | Specifies on which side render modal: One of: bottom-left, bottom-right, top-left, top-right, left-top, left-bottom, right-top, right-bottom
`modalStyle` | object, array | ` ` | Style of the modal
`modalHeight` | number | `165` | Height of the Modal
`modalWidth` | number | `250` | Width of the Modal
`modalBackgroundColor` | string | `#ffffff` | Background Color of the Modal
`inactiveIconColor` | string | `#2dbecd ` | Color of the inactive icon
`activeIconColor` | string | `#208590` | Color of the active icon
`iconSize` | number | `24` | Size of the Icon
`onIconPress` | func | `() => {}` | Function called after icon pressed
`headlineType` | string | `H6` | Type of the Headline component
`headlineLabel` | string | `Headline` | Headline text
`headlineStyle` | object, array | `{}` | Style of the Headline component
`headlineColor` | string | `#1b1b25` | Color of the Headline component
`headlineFontFamily` | string | `Lato-Black` | Font family of the Headline component
`paragraphType` | string | `XSmall` | Type of the Paragraph component
`paragraphText` | string | `Lorem Ipsum..` | Text of the Paragraph component
`paragraphStyle` | object, array | `{}` | Style of the Paragraph component
`paragraphColor` | string | `#1b1b25` | Color of the Paragraph component
`paragraphFontFamily` | string | `Lato-Regular` | Font Family of the Paragraph component
`numberOfParagraphLines` | number | `4` | Number of lines of Paragraph component
</details>


# Modules

<details><summary><b>Date Pickers</b></summary>

  <details><summary><b>Simple date picker</b></summary>

   ## **Simple date picker**

   > Simple date picker is a ready component which has formated data and validation rules for date mark. Simple date picker is build with TextField component.

   ```jsx
   import React, { Component } from 'react'

   import { SimpleDatePicker } from 'liquid-design-react-native'

   class Example extends Component {
     render() {
       return (
         <SimpleDatePicker />
       )
     }
   }
   ```
   ### Available Props
   Prop                | Type    |  Default   | Description
   ------------------- | ------- | ---------- | -----------
   `disabled` | bool | `false` | If true will display component with disabled state.
   `withLabel` | bool | `true` | If true will display label element above text input.
   `labelText` | string | `Start date` | Text of the label element.
   `errorMessage` | string | `* Error Message` | Text of the error message when incorrect data is provided.
   `onEndEditing` | func | `() => {}` | Function called after end editing text input.
  </details>

  <details><summary><b>Single date picker</b></summary>

   ## **Single date picker**

   > Single date picker is a ready component which has formated data, validation rules and calendar for date mark. Single date picker is build with TextField and SmallCalendar components.

   ```jsx
   import React, { Component } from 'react'

   import { SingleDatePicker } from 'liquid-design-react-native'

   class Example extends Component {
     render() {
       return (
         <SingleDatePicker />
       )
     }
   }
   ```
   ### Available Props
   Prop                | Type    |  Default   | Description
   ------------------- | ------- | ---------- | -----------
   `disabled` | bool | `false` | If true will display component with disabled state.
   `withLabel` | bool | `true` | If true will display label element above text input.
   `labelText` | string | `Start date` | Text of the label element.
   `errorMessage` | string | `* Error Message` | Text of the error message when incorrect data is provided.
   `onEndEditing` | func | `() => {}` | Function called after end editing text input.
   `calendarXPosition` | number | ` ` | If provided will display calendar element on different position on x axis.
   </details>

  <details><summary><b>Multi date picker</b></summary>

   ## **Multi date picker**

   > Multi date picker is a ready component which has formated data, validation rules and calendar for date mark. Multi date picker is build with TextField and SmallCalendar components.

   ```jsx
   import React, { Component } from 'react'

   import { MultiDatePicker } from 'liquid-design-react-native'

   class Example extends Component {
     render() {
       return (
         <MultiDatePicker />
       )
     }
   }
   ```
   ### Available Props
   Prop                | Type    |  Default   | Description
   ------------------- | ------- | ---------- | -----------
   `disabled` | bool | `false` | If true will display component with disabled state.
   `withLabel` | bool | `true` | If true will display label element above text input.
   `labelStartText` | string | `Start date` | Text of the label element (first input).
   `labelEndText` | string | `End date` | Text of the label eleme (second input).
   `errorMessage` | string | `* Error Message` | Text of the error message when incorrect data is provided.
   `onEndStartEditing` | func | `() => {}` | Function called after end editing text input (start date).
   `onEndEndStartEditing` | func | `() => {}` | Function called after eediting text input (end date).
   `firstDay` | string | `1` (Monday) | Start of the week.
  </details>

</details>

<details><summary><b>FAQs</b></summary>

## **FAQs**

> FAQ is a ready module which enables to make headline with description and sections of questions. FAQ contains Headline and Accordion components.

```jsx
import { FAQ } from '@liquid-design/liquid-design-react-native'

const MULTIPLE_SECTIONS = [
  {
    title: 'Section Title',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam  eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  },
  {
    title: 'Section Title',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam  eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  },
  {
    title: 'Section Title',
    content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam  eiusmod tempor incididunt ut labore et dolore magna aliqua.'
  }
]

class Example extends Component {
  state = {
    activeSections: []
  }

  setSections(sections) {
    this.setState({ activeSections: sections })
  }

  render() {
    return (
      <FAQ
        expandMultiple
        onChange={this.setSections.bind(this)}
        activeSections={this.state.activeSections}
        sections={MULTIPLE_SECTIONS}
        headlineText='Headline Title'
        description='Do you have any questions?'
      />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`expandMultiple` | bool | `false` | If provided, will display a multiple accordions.
`activeSections` | [number] || string | ` ` | Control which index in the sections array is currently open.
`sections` | array | `(isRequired)` | An array of sections passed to the render methods.
`initiallyActiveSection` | number | ` ` | If provided, will open chosen section on start.
`onChange` | func | ` ` | Called everytime when one of sections is clicked.
`width` | number | `335` | Width of the accordion.
`duration` | number | `200` | Duration of open/hide content animation.
`onAnimationEnd` | func | `() => {}` | Callback that is called when the animation is ended.
`disabled` | bool | `false` | Determines when disabled is displayed.
`borderColor` | string | `#e9e9ed` | The color used for the borders on top and bottom.
`titleFontFamily` | string | `Lato Black` | Font family of section title.
`titleFontSize` | number | `16` | Font size of the section title.
`titleFontWeight` | number | ` ` | Font weight of the section title.
`titleLineHeight` | number | `20` | Line height of the section title.
`inactiveTitleColor` | string | `#1b1b25` | Color of the section title when section is closed.
`activeTitleColor` | string | `#2dbecd` | Color of the section title when section is opened.
`contentFontFamily` | string | `Lato Regular` | Font family of section content.
`contentFontSize` | number | `14` | Font size of the section content.
`contentFontWeight` | number | ` ` | Font weight of the section content.
`contentLineHeight` | number | `24.5` | Line height of the section content.
`contentColor` | string | `#1b1b25` | Color of the section content.
`contentWidth` | string | `100%` | Width of the section content.
`contentHeight` | number | `125` | Height of the section content, will remove word-break effect when animating (iOS only).
`moduleWidth` | string | `100%` | Width of the Headline and description section.
`descriptionFontFamily` | string | `Lato Regular` | Font family of the description.
`descriptionFontSize` | number | `14` | Font size of the description.
`descriptionLineHeight` | number | `24.5` | Line height of the description.
`descriptionColor` | string | `#a4a4ae` | Color of the description.
`description` | string | ` ` | Text of the description under Headline.
`headlineType` | string | `H3` | Type of the Headline component.
`headlineText` | string | `Headline Insert` | Text of the Headline.
`headlineColor` | string | `#a4a4ae` | Color of the Headline.
</details>

<details><summary><b>Footers</b></summary>

## **Footers**

> Footer is a ready module which enables to make headline with labels and icons. Footer contains Headline component. Labels and icons can be clickable.

```jsx
import { Footer } from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <Footer
        labels={[
          { key: 1, title: 'Label Text', onPress: () => {} },
          { key: 2, title: 'Label Text', onPress: () => {} },
          { key: 3, title: 'Label Text', onPress: () => {} }
        ]}
      />
    )
  }
}
```

### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`labels` | arrayOf(shape({ key: number, title: string, onPress: func})) | `(isRequired)` | Array of label objects.
`labelFontFamily` | string | `Lato Regular` | Font family of the label.
`labelFontSize` | number | `12` | Font size of the label.
`labelLineHeight` | number | `15` | LineHeight of the label.
`labelColor` | string | `#1b1b25` | Color of the label.
`headlineType` | string | `H4` | Type of the headline element.
`headlineText` | string | `Get started today(...)` | Text of the headline element.
`lineColor` | string | `#d5d5d9` | Color of the line element.
`lineHeight` | number | `1` | Height of the line element.
`lineWidth` | string | `100%` | Width of the line element.
`footerWidth` | number | `device width` | Width of the whole footer.
`backgroundColor` | string | `#f8f8fc` | Background color of the whole footer.
`primaryIconName` | string | `placeholder` | Name of the primary icon (single on left).
`primaryIconSize` | number | `24` | Size of the primary icon (single on left).
`primaryIconColor` | string | `#1b1b25` | Size of the primary icon (single on left).
`onPrimaryIconPress` | func | `() => {}` | Called everytime when primary icon (single on left) is clicked.
`secondaryLeftIconName` | string | `placeholder` | Name of the secondary icon (left).
`secondaryLeftIconSize` | number | `24` | Size of the secondary icon (left).
`secondaryLeftIconColor` | string | `#1b1b25` | Size of the secondary icon (left).
`onSecondaryLeftIconPress` | func | `() => {}` | Called everytime when secondary icon (left) is clicked.
`secondaryCenterIconName` | string | `placeholder` | Name of the secondary icon (center).
`secondaryCenterIconSize` | number | `24` | Size of the secondary icon (center).
`secondaryCenterIconColor` | string | `#1b1b25` | Size of the secondary icon (center).
`onSecondaryCenterIconPress` | func | `() => {}` | Called everytime when secondary icon (center) is clicked.
`secondaryRightIconName` | string | `placeholder` | Name of the secondary icon (right).
`secondaryRightIconSize` | number | `24` | Size of the secondary icon (right).
`secondaryRightIconColor` | string | `#1b1b25` | Size of the secondary icon (right).
`onSecondaryRightIconPress` | func | `() => {}` | Called everytime when secondary icon (right) is clicked.
</details>

<details><summary><b>Forms</b></summary>

## **Forms**

> Forms can be constructed with [TextField](#elements), [Checkbox](#elements), [Button](#elements), and [Dropdown](#elements) elements. The logic of the forms is up to the user, this library provides just the UI components.

```jsx
import React, { Component } from 'react'

import {
  TextField,
  Checkbox,
  GhostButton,
  Button
} from '@liquid-design/liquid-design-react-native'

class Example extends Component {

  render() {
    return (
      <View>
        <TextField
          placeholder="john.doe@internet.com"
          textInputLabel="E-Mail Adress"
        />
        <TextField
          placeholder="Enter your password"
          textInputLabel="Password"
        />
        <Checkbox
          checked={false}
          title="Keep me logged in" />
        <GhostButton title="Cancel" />
        <Button
          title="Login"
        />
      </View>
    )
  }
}
```
</details>

<details><summary><b>Light Boxes</b></summary>

## **Light Boxes**

> Light Box is a component to create a fancy box for content utilizing the Modal component.

```jsx
import React, { Component } from 'react'

import LightBox from '@liquid-design/liquid-design-react-native'

class Example extends Component {
  state = {
    visibleModal: null
  }

  renderButton = (text, onPress) => (
    <Button
      title={text}
      onPress={onPress}
    />
  )

  render() {
    return (
      {this.renderButton('Default modal', () =>
      this.setState({ visibleModal: 1 }))}
      <LightBox
        isVisible={this.state.visibleModal === 1}
        contentText='Example text'
        buttonText='Accept'
        cancelText='Cancel'
        onCancelPress={() => this.setState({ visibleModal: null })}
      />
      }
    )
  }
}
```
### Available Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`isVisible` | bool | `false` | Determines when Light Box is displayed.
`needScroll` | bool | `false` | If provided, will display a Light Box in ScrollView.
`duration` | number | `200` | Duration of animation on show/close Modal.
`backdropColor` | string| `#1b1b25` | Color of the backdrop.
`backdropOpacity` | number | `0.7` | Opacity of the backdrop.
`onBackButtonPress` | func | `() => {}` | Function called on request close.
`withGraphic` | bool | `false` | If provided, will display a different styled component - with image.
`withCta` | bool | `false` | If provided, will display a different styled component - with buttons.
`withTextField` | bool | `false` | If provided, will display a different styled component - with text field.
`headerTitle` | string | `Header Label` | Title of the header.
`headlineText` | string | `Headline Text` | Headline of the content.
`headlineType` | string | `H3` | Type of the headline.
`contentText` | string | `Lorem ipsum dolor sit amet, consetetur sadipscing elitr(...)` | Text of the content.
`buttonText` | string | `Button Text` | Title of the primary button.
`cancelText` | string | `Cancel Text` | Title of the secondary button.
`onButtonPress` | func | `() => {}` | Function called when primary button is pressed.
`onCancelPress` | func | `() => {}` | Function called when secondary button or closing icon is pressed.
`placeholder` | string | `Add Placeholder Text here` | Text of the placeholder in modal with TextField.
`errorMessage` | string | `Error Message` | Text of the error message in modal with TextField.
`onError` | bool | `false` | Determines when error message is displayed in modal with TextField.
`onChangeText` | func | `() => {}` | Function called when text is changes in modal with TextField.
`onFocus` | func | `() => {}` | Function called when TextField is focused in modal with TextField.
`onBlur` | func | `() => {}` | Function called when TextField is blurred in modal with TextField.
`modalWidth` | number | `350` | Width of the modal.
`imagePath` | node | ` ` | Path of the image in modal with Graphic.
`contentBgColor` | string | `#ffffff` | Background color of the modal content.
`contentColor` | string | `#1b1b25` | Color of the modal content text.
`contentFontFamily` | string | `Lato Regular` | Font family of the modal content text.
`contentFontWeight` | number | ` ` | Font weight of the modal content text.
`contentFontSize` | number | `16` | Font size of the modal content text.
`contentLineHeight` | number | `28` | Line height of the modal content text.
`headerBgColor` | string | `#f8f8fc` | Backgroud color of the modal header.
`headerTitleColor` | string | `#1b1b25` | Color of the modal header title.
`headerFontFamily` | string | `Lato Regular` | Font family of the modal header title.
`headerFontWeight` | number | ` ` | Font weight of the modal header title.
`headerFontSize` | number | `12` | Font size of of the modal header title.
`headerLineHeight` | number | `15` | Line height of the modal header title.
`iconSize` | number | `20` | Size of the closing X icon.
</details>

<details><summary><b>Headers</b></summary>

## **Headers**

> Header is a ready module which enables to make the intros of every screen. Header contains Search bar component.

```jsx
import { Header } from '@liquid-design/liquid-design-react-native/src'

const searchOptions = [
  { text: 'Search Result 1 Search Result 1 Search Result 1' },
  { text: 'Search Result 2' },
  { text: 'Search Result 3' },
  { text: 'Search Result 4' },
  { text: 'Search Result 5' },
  { text: 'Search Result 6' },
  { text: 'Search Result 7' }
]

class Example extends Component {

  constructor(props) {
    super(props)
    this.state = {
      searchTermDefault: ''
    }
  }

  render() {
    return (
      <Header
        searchOptions={searchOptions}
        searchProperty='text'
        searchTerm={this.state.searchTermDefault}
        onChangeText={(text) => {
        this.setState({ searchTermDefault: text })
        }}
      />
    )
  }
}
```

### Avaliable Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`headerWidth` | number | `device width` | Width of the whole Header module.
`headerHeight` | number | `60` | Height of the whole Header module.
`backgroundColor` | string | `#2dbecd` | Background color of the whole Header module.
`logoColor` | string | `#ffc832` | Color of the logo element.
`searchOptions` | array | `(isRequired)` | Array of the options to search.
`searchTerm` | string | ` ` | Search term for search bar component.
`onChangeText` | func | `() => {}` | Function called when text is changed in Search bar.
`searchProperty` | string | `(isRequired)` |Property which specify by which property search array.
`withIcons` | bool | `false` | If provided, will display a Header with icons.
`withButton` | bool | `false` | If provided, will display a Header with button.
`withLabel` | bool | `false` | If provided, will display a Header with labels.
`iconsColor` | string | `#ffffff` | Colors of the icons in Header with icons.
`buttonIconName` | string | `placeholder` | Name of the icon in Header with button.
`leftIconName` | string | `bell` | Name of the left icon in Header with icons.
`middleIconName` | string | `information1` | Name of the middle icon in Header with icons.
`rightIconName` | string | `settings` | Name of the right icon in Header with icons.
`onLeftIconPress` | func | `() => {}` |Function called when left icon is pressed in Header with icons.
`onMiddleIconPress` | func | `() => {}` |Function called when middle icon is pressed in Header with icons.
`onRightIconPress` | func | `() => {}` |Function called when right icon is pressed in Header with icons.
`labelFontFamily` | string | `Lato Regular` Font family of the label text in Header with labels.
`labelFontSize` | number | `12` | Font size of the label text in Header with labels.
`labelLineHeight` | number | `15` | Line height of the label text in Header with labels.
`labelColor` | string | `#ffffff` | Color of the label text in Header with labels.
`label1` | string | ` ` | Text of the first label in Header with labels.
`label2` | string | ` ` | Text of the second label in Header with labels.
</details>

<details><summary><b>Navigations</b></summary>

## **Navigations**

> Navigation is a ready module that allows to create a drawer with customizable elements

```jsx
import React, { Component } from 'react'

import Navigation from '@liquid-design/liquid-design-react-native'

class Example extends Component {
  constructor(props) {
    super(props)
    this.state = {
      open: true,
      tabs: [
        {
          title: 'title-01',
          icon: 'placeholder',
          label: 'Nav title',
          contentView: this.contentView()
        },
        {
          title: 'title-02',
          icon: 'placeholder',
          label: 'Nav title',
          contentView: this.contentView()
        },
        {
          title: 'title-03',
          icon: 'placeholder',
          label: 'Nav title',
          contentView: this.contentView()
        }
      ]
    }
  }

  contentView = () => (
    <View>
      <Text> Tab content </Text>
    </View>
  )

  onDrawerClose = () => {
    this.setState({ open: false })
  }

  render() {
    const { tabs, open } = this.state
    return (
      <Navigation
        imagePath='src/assets/image.png'
        title='Title 01'
        tabs={tabs}
        open={open}
        onClose={this.onDrawerClose}
      />
    )
  }
}
```
### Avaliable Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`imagePath` | node |  | Path to the image - visible in the bottom part of the navigation.
`title` | string |  | Title of the menu - visible in the bottom part of the navigation.
`tabs` | array | `[]` | Array of navigation items - objects with title, icon and label.
`isOpen` | bool | false | Value that determines whether the navigation is opened or closed.
`openMenuOffset` | number | 60 | Value that determines the offset of the menu.
`edgeHitWidth` | number | 300 | Value that determines the width of the swipable region.
`bounceBackOnOverdraw` | bool | false | Value that tells whether to bounce back when overswiped or not.
`onClose` | func |  | Function that determines what should happen when the navigation closes.
`navigationBody.navigationFontFamily` | string | Default bold font | Font family of the navigation
`navigationBody.navigationFontSize` | number | 7.5 | Font size of the navigation
`navigationBody.navigationFontColor` | string | '#a4a4ae' | Font color of the navigation
`navigationBody.navigationLineHeight` | number | 9.4 | Line height of the navigation
`navigationBody.activeTabFontFamily` | string | Default bold font | Font family of the active tab
`navigationBody.activeTabFontColor` | string | '#2dbecd' | Font color of the active tab
`navigationBody.tabFontFamily` | string | Default regular font | Font family of the inactive tab
`navigationBody.tabFontSize` | number | 10 | Font size of the inactive tab
`navigationBody.tabFontColor` | string | '#a4a4ae' | Font color of the inactive tab
`navigationBody.tabLineHeight` | number | 9.4 | Line height of the inactive tab
`navigationBody.iconColor` | string | '#a4a4ae' | Color of inactive icon
`navigationBody.iconSize` | number | 24 | Size of icons
`navigationBody.activeIconColor` | string | '#2dbecd' | Color of active icons
</details>

<details><summary><b>Small Calendars</b></summary>

## **Small Calendars**

> Small Calendar is a ready module that allows to create a custom calendar with appointments

```jsx
import React, { Component } from 'react'

import SmallCalendar from '@liquid-design/liquid-design-react-native'

class Example extends Component {
  render() {
    return (
      <SmallCalendar
        firstDay='2018-10-10'
        appointments={{
          '2018-10-06': { time: '16:00', description: 'dinner with Mom' }
        }}
      />
    )
  }
}
```
### Avaliable Props
Prop                | Type    |  Default   | Description
------------------- | ------- | ---------- | -----------
`firstDay` | string | `1` (Monday) | Start of the week.
`appointments` | object | `{}` | Object of appointments where keys are date strings in ISO 8601 format and values are objects of the shape: { time: '16:00', description: 'dinner with Mom' }.
</details>


# Contributing
see [CONTRIBUTING.md](CONTRIBUTING.md)

### Technology stack
| Technology     | Version          |
|----------------|------------------|
| React          | 16.3.2           |
| React Native   | 0.50.0           |
